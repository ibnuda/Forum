module Clie.Pages.Login.Views

open System

open Fable.Core
open Fable.Core.JsInterop
open Fable.Import
open Fable.Helpers.React
open Fable.Helpers.React.Props

open Clie.Messages
open Clie.Utility.Style

open Types

let view (model : LoginModel) (dispatch : AppMessage -> unit) =
  match model.State with
  | LoginState.LoggedInState token ->
    div [ Id "greeting" ] [
      h3 [ ClassName "title is-2 has-text-centered" ] [
        text [ ] [ str <| sprintf "Hello %s" model.LoginReq.username ]
        text [ ] [ str <| sprintf "Token %A" token ]
      ]
    ]
  | LoginState.LoggedOutState ->
    div [ ClassName "column is-half is-offset-one-quarter" ] [
      div [ ClassName "field" ] [
        h2 [ ClassName "title is-2 has-text-centered" ] [
          text [ ] [ str "Login" ]
        ]
        div [ ClassName "field" ] [
          label [ ClassName "label" ] [
            str "Name"
          ]
          div [ ClassName "control" ] [
            input [ Id "username"
                    ClassName "input"
                    Placeholder "Username"
                    Value <| U2.Case1 model.LoginReq.username
                    OnInput (fun event ->
                      unbox event.target?value
                      |> LoginMessages.SetUsername
                      |> LoginMsg
                      |> dispatch)
                    HTMLAttr.Type "text"
                    AutoFocus true ]
          ]
        ]
        div [ ClassName "field" ] [
          label [ ClassName "label" ] [
            str "Password"
          ]
          div [ ClassName "control" ] [
            input [ Id "password"
                    ClassName "input"
                    Placeholder "Username"
                    Value <| U2.Case1 model.LoginReq.password
                    OnInput (fun event ->
                      unbox event.target?value
                      |> LoginMessages.SetPassword
                      |> LoginMsg
                      |> dispatch)
                    HTMLAttr.Type "password" ]
          ]
        ]
        div [ ClassName "field is-grouped" ] [
          div [ ClassName "control is-pulled-right" ] [
            button [ ClassName "button is-primary"
                     OnClick (fun _ -> dispatch (LoginMsg ClickLogin)) ] [
              text [ ] [ str "Log In" ]
            ]
          ]
        ]
      ]
    ]
