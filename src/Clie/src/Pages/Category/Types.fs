module Clie.Pages.Category.Types

open System

type Category =
  { id         : string
    name       : string
    desc       : string
    threads    : int
    posts      : int
    lastthread : string
    lastuser   : string
    lastactive : DateTime }

type Categories =
  { categories : Category list }

type CategoryModel =
  { categories : Categories
    ErrorMessa : string }
  static member Empty =
    { categories = { categories = [ ] }
      ErrorMessa = "" }

let setcateg (categ : Categories) model : CategoryModel =
  { model with categories = categ }

let seterror error model =
  { model with ErrorMessa = error }
