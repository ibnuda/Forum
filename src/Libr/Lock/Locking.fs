namespace Forum

open Chessie.ErrorHandling

open Unif.Errors
open Unif.ForumModels
open Unif.Auth

module Locking =

  open Utilities
  open Tokenization
  open DocReadWrite

  open LockPreparations

  [<RequireQualifiedAccessAttribute>]
  module Lock =

    let createToken userRequestInJson expiredIn =
      userRequestInJson
      |>  userRequestFromJson
      >>= checkUser
      >>= userToJWT expiredIn

    let private getUserFromToken (token : Token) =
      match Read.load<ForumUser> token.UserId with
      | Some user -> (token, user) |> ok
      | None ->  NonExistentUser token.UserId |> fail

    let private compareQualifFromTokenAndUser (user : ForumUser) (qualif : Qualification) (token : Token) =
      if user.Qualification = qualif then
        (token, user) |> ok
      else
        TokenNotKosher |> fail

    let private compareExpirationTime timeInL (token : Token) (_ : ForumUser) =
      if token.ExpiresIn > timeInL then
        token |> ok
      else
        TokenExpired |> fail

    let checkTokenValidity timeInL (token : Token) =
      getUserFromToken token >>= fun (token, user) ->
      compareQualifFromTokenAndUser user token.Quali token >>= fun (token, user) ->
      compareExpirationTime timeInL token user

    let validateJwt timeInL jwt =
      jwt
      |>  jwtToToken
      >>= checkTokenValidity timeInL

    let authorize (header : string option) =
      match header with
      | Some header ->
        validateJwt (nao ()) header |> (failed >> not)
      | None -> false

    let isQualified required (header : string option) =
      match header with
      | Some header ->
        match validateJwt (nao ()) header with
        | Ok (token, _) -> token.Quali.Value >= required.Value
        | Bad _ -> false
      | None -> false
