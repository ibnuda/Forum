namespace Forum

open System

open Chessie.ErrorHandling
open Marten

open Unif.Errors
open Unif.ForumModels
open Unif.MainModels

module ManageUser =

  open BusinessDefaults
  open BusinessRules
  open DocHelpers
  open DocReadWrite

  let checkByUsername username =
    use session = store.LightweightSession ()
    session
    |> Session.query<ForumUser>
    |> Queryable.filter <@ fun u -> u.Username = username @>
    |> Queryable.tryExactlyOne

  let createUser id username password =
    match checkByUsername username with
    | Some _ -> UsernameAlreadyExists username |> fail
    | None   ->
      let createdUser = createNormalUser id username password
      Write.writeEntity createdUser
      |> ShownUser.CreateFromForumUser
      |> ok

  let changePasswordUser execId userId newpass =
    match Read.load<ForumUser> execId,
          Read.load<ForumUser> userId with
    | None, _                -> NonExistentUser execId |> fail
    | Some _, None           -> NonExistentUser userId |> fail
    | Some userA, Some userB ->
      editPasswordUser userA userB newpass
      |> lift Write.writeEntity

  let private modifyUser f userAId userBId =
    match Read.load<ForumUser> userAId,
          Read.load<ForumUser> userBId with
    | None, _                -> NonExistentUser userAId |> fail
    | Some _, None           -> NonExistentUser userBId |> fail
    | Some userA, Some userB ->
      f userA userB
      |> lift Write.writeEntity
      |> lift ShownUser.CreateFromForumUser

  let makeAdministrator = modifyUser makeAdmin
  let makeSenModerator  = modifyUser grantSeniorModeration
  let makeModerator     = modifyUser grantModeration
  let makeUserBanned    = modifyUser banUser
  let makeUserFree      = modifyUser releaseBan
  let deleteSenMod      = modifyUser depriveSeniorModeration
  let deleteMod         = modifyUser depriveModeration

  let checkExistenceAdmin () =
    checkByUsername "Administrator" |> Option.isSome

  let createAdministrator password =
    ForumUser.Create (Guid.NewGuid (), "Administrator", password, admin)
    |> Write.writeEntity
