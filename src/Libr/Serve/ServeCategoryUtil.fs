
namespace Forum

open System

open Chessie.ErrorHandling
open Marten

open Unif.ForumModels
open Unif.MainModels

module ServeCategoryUtil =

  open Utilities
  open DocHelpers
  open ServeThread
  open ServeUser

  let categoryThreads categoryId =
    use session = store.QuerySession ()
    session
    |> Session.query<ForumThread>
    |> Queryable.count <@ fun t -> t.CategoryId = categoryId @>

  let categoryReplies categoryId =
    use session = store.QuerySession ()
    session
    |> Session.query<ForumThread>
    |> Queryable.filter <@ fun t -> t.CategoryId = categoryId @>
    |> Seq.sumBy (fun t -> t.Replies.Length)

  let lasts categoryId =
    use session = store.QuerySession ()
    session
    |> Session.query<ForumThread>
    |> Queryable.filter <@ fun t -> t.CategoryId = categoryId @>
    |> Queryable.orderByDescending <@ fun thr -> thr.LastActive @>
    |> Queryable.tryHead
    |> fun res ->
      match res with
      | None  -> ("Empty", DateTime.MinValue, Guid.Empty)
      | Some thr -> (thr.Title, thr.LastActive, thr.LastBy)

  let threadsInCats catId =
    use session = store.QuerySession ()
    session
    |> Session.query<ForumThread>
    |> Queryable.filter <@ fun t -> t.CategoryId = catId @>
    |> Queryable.orderByDescending <@ fun thr -> thr.LastActive @>
    |> Seq.toList

  let threadIdsInCats catId =
    use session = store.QuerySession ()
    session
    |> Session.query<ForumThread>
    |> Queryable.filter <@ fun t -> t.CategoryId = catId @>
    |> Queryable.orderByDescending <@ fun thr -> thr.LastActive @>
    |> Queryable.map <@ fun thr -> thr.Id @>
    |> Seq.toList

  let toListedCategory (cat : ForumCategory) =
    let id           = cat.Id
    let name         = cat.Name
    let desc         = cat.Description
    let threadsCount = categoryThreads cat.Id
    let repliesCount = categoryReplies cat.Id
    let lt, la, lb   = lasts cat.Id
    let lastuser     =
      if lb = Guid.Empty then
        "Empty"
      else
        getUserName lb
    { Id         = id
      Name       = name
      Desc       = desc
      Threads    = threadsCount
      Posts      = repliesCount
      LastThread = lt
      LastUser   = lastuser
      LastActive = la }

  let toShownCategory (cat : ForumCategory) =
    trial {
      let id        = cat.Id
      let name      = cat.Name
      let desc      = cat.Description
      let! threads  =
        threadsInCats cat.Id
        |> List.map toListedThread
        |> collect
      return
        ShownCategory.Create (id, name, desc, threads)
    }

  let toApiShownCategory (cat : ForumCategory) =
    let id       = cat.Id
    let name     = cat.Name
    let desc     = cat.Description
    let threads  = threadIdsInCats cat.Id
    ApiShownCategory.Create (id, name, desc, threads)

  let toChunkedShownCategory page perPage (cat : ForumCategory) =
    trial {
      let id = cat.Id
      let name = cat.Name
      let desc = cat.Description
      let! threads =
        threadIdsInCats cat.Id
        |>  chunkList page perPage cat.Id
        >>= (List.map getThread >> collect)
        >>= (List.map toListedThread >> collect)

      return
        ShownCategory.Create (id, name, desc, threads)
    }

  let toChunkedApiShownCategory page perPage (cat : ForumCategory) =
    trial {
      let id       = cat.Id
      let name     = cat.Name
      let desc     = cat.Description
      let! threads = threadIdsInCats cat.Id |> chunkList page perPage cat.Id
      return
        ApiShownCategory.Create (id, name, desc, threads)
    }
