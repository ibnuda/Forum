module Clie.Pages.Menu.Types

open Elmish

open Clie.Messages
open Clie.Utility.Utility

type MenuModel =
  { User : UserAuth option }
