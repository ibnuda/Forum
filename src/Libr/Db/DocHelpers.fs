namespace Forum

open Chessie.ErrorHandling

open Marten
open Npgsql

open Unif.ForumModels

module DocHelpers =

  open ConfigRead
  open ImagesModels

  let createConnectionString host username password database =
    sprintf "Host=%s;Username=%s;Password=%s;Database=%s" host username password database
    |> NpgsqlConnectionStringBuilder

  let config =
    match readConfig () with
    | Ok (r, _) -> r
    | Bad _     -> failwith "LOL"

  let private host = config.Db.Host
  let private user = config.Db.User
  let private pass = config.Db.Pass
  let private name = config.Db.Name

  let connection = createConnectionString host user pass name |> string

  let store =
    DocumentStore.For(fun doc ->
      doc.AutoCreateSchemaObjects <- AutoCreate.CreateOrUpdate
      doc.Connection connection
      doc.Logger (ConsoleMartenLogger())
      doc.Schema.For<Qualification>().Index(fun x -> x.Id :> obj) |> ignore
      doc.Schema.For<Activity>().Index(fun x -> x.Id :> obj) |> ignore
      doc.Schema.For<ForumUser>().Index(fun x -> x.Id :> obj) |> ignore
      doc.Schema.For<ForumPost>().Index(fun x -> x.Id :> obj) |> ignore
      doc.Schema.For<ForumThread>().Index(fun x -> x.Id :> obj) |> ignore
      doc.Schema.For<ForumCategory>().Index(fun x -> x.Id :> obj) |> ignore
      doc.Schema.For<ImageInDb>().Index(fun x -> x.Id :> obj) |> ignore
    )
