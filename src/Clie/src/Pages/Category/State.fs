module Clie.Pages.Category.State

open System

open Fable.Core
open Fable.Core.JsInterop
open Fable.Import

open Fable.PowerPack
open Fable.PowerPack.Fetch.Fetch_types

open Elmish

open Clie.Utility.Utility
open Clie.Messages

open Types

let private get () = getItem "categories" None

let private getCat () =
  Cmd.ofPromise get () CategoryGet CatNotFound

let init () : CategoryModel * Cmd<CategoryMessages> =
  CategoryModel.Empty,
  getCat ()

let update (message : CategoryMessages) (model : CategoryModel) =
  match message with
  | CategoryMessages.CategoryGet something ->
    model
    |> setcateg (ofJson<Categories> something),
    getCat ()
  | CategoryMessages.CatNotFound ex ->
    model
    |> seterror ex.Message,
    getCat ()
  | CategoryMessages.ClickCatLink ->
    model,
    getCat ()
