module Clie.Pages.Thread.Views

open System

open Elmish

open Fable.Helpers.React
open Fable.Helpers.React.Props

open Clie.Global
open Clie.Messages

open Types

let view (model : ThreadModel) dispatch =
  match String.IsNullOrEmpty model.ErrMessage with
  | false ->
    div [ ] [
      h3 [ ] [
        str model.ErrMessage
      ]
    ]
  | true ->
    div [ ] [
      for post in model.PostsData do
        yield
          div [ ClassName "box" ] [
            article [ ClassName "media" ] [
              div [ ClassName "media-left" ] [
                figure [ ClassName "image is-64x64" ] [
                  a [ Href (toHash <| Page.UserInfo post.author.id) ] [
                    img [ Src "/assets/images/128x128.png"
                          Alt post.author.username ]
                  ]
                ]
              ]
              div [ ClassName "media-content" ] [
                div [ ClassName "content" ] [
                  p [ ] [
                    str post.content
                  ]
                ]
                nav [ ClassName "level" ] [
                  div [ ClassName "level-right" ] [
                    a [ ClassName "level-item"
                        Href (toHash <| Page.SinglePost post.id )] [
                      span [ ClassName "icon is-small" ] [
                        i [ ClassName "fa fa-link" ] [ ]
                      ]
                    ]
                  ]
                ]
              ]
            ]
          ]
    ]
