namespace Serv

open Freya.Core
open Operators
open Freya.Machines.Http
open Freya.Machines.Http.Cors
open Freya.Types.Http
open Freya.Routers.Uri.Template

open Forum.BusinessDefaults

open Config

module internal RepRes =

  open Unif.Errors
  open Unif.ResponseModels

  let repFile (file : byte []) =
    { Description =
        { Charset = Some Charset.Utf8
          Encodings = None
          MediaType = None
          Languages = None }
      Data = file }

  let jRes (j : Chessie.ErrorHandling.Result<ForumResponse, Failures>) =
    { Description =
        { Charset = Some Charset.Utf8
          Encodings = None
          MediaType = Some MediaType.Json
          Languages = None }
      Data = System.Text.Encoding.UTF8.GetBytes (resToJson j)}

  let json (j : string) =
    { Description =
        { Charset = Some Charset.Utf8
          Encodings = None
          MediaType = Some MediaType.Json
          Languages = None }
      Data = System.Text.Encoding.UTF8.GetBytes j }

module RoutesRes =

  open ReqHandlRes


  let common =
    freyaHttpMachine {
      cors
      corsEnabled true
      corsSupportsCredentials true
      corsMethods [ POST; GET; PUT; OPTIONS ]
      handleUnauthorized (RepRes.json >> Freya.init <| """{"error":"Forbidden."}""")
      handleNotFound (RepRes.json >> Freya.init <| """{"error":"The thing have you posted to does not exist."}""")
      handleInternalServerError (RepRes.json >> Freya.init <| "There's something wrong in the server.")
    }

  let private routeServeGet checker handler =
    freyaHttpMachine {
      methods [ GET; OPTIONS ]
      exists checker
      handleOk (RepRes.jRes <!> handler)
      including common
    }

  let private routeCreate handler =
    freyaHttpMachine {
      methods [ POST; OPTIONS ]
      created true
      handleCreated (RepRes.jRes <!> handler)
      including common
    }

  let routeForUser =
    freyaHttpMachine {
      methods [ GET; PUT; OPTIONS ]
      authorized (Util.minPut moderator)
      exists Util.userExists
      doPut (ignore <!> changeUserQH)
      handleOk (RepRes.jRes <!> userGetH)
      including common
    }

  let routeForPost =
    freyaHttpMachine {
      methods [ GET; PUT; OPTIONS ]
      authorized (Util.minPut normaluser)
      exists Util.postExists
      doPut (ignore <!> postEditH)
      handleOk (RepRes.jRes <!> postGetH)
      including common
    }

  let routeStandard qu ex okH creH putDo =
    freyaHttpMachine {
      methods [ GET; POST; PUT; OPTIONS ]
      authorized (Util.minPut qu)
      created (Util.isCreated creH)
      exists ex
      handleOk (RepRes.jRes <!> okH)
      handleCreated (RepRes.jRes <!> okH)
      doPut (ignore <!> putDo)
      including common
    }

  let routeAuthCreate = routeCreate tokenCreateH
  let routeUserCreate = routeCreate userCreateH

  let routeCategoryCreate =
    freyaHttpMachine {
      authorized (Util.minCreate admin)
      methods [ POST; OPTIONS ]
      created (Util.isCreated catCreateH)
      handleCreated (RepRes.jRes <!> catCreateH)
      including common
    }

  let routeCategoryMain   = routeServeGet (Freya.init true) catListH

  let routeCategory  = routeStandard senmod     Util.cateExists catGetH thrCreateH gembokCatH
  let routeThread    = routeStandard normaluser Util.threExists thrGetH thrReplyH  gembokThrH
  let routeUserPostsGet   = routeServeGet Util.userExists userPostGetH
  let routeUserThreads    = routeServeGet Util.userExists userThreGetH

  let routeLost =
    freyaHttpMachine {
      exists false
      including common
    }

  let rootWithConfig (config : ServConfig) =
    freyaRouter {
      resource config.Path.Root            routeCategoryCreate
      resource config.Path.Token           routeAuthCreate
      resource config.Path.Users.SignUp    routeUserCreate
      resource config.Path.Users.Root      routeForUser
      resource config.Path.Users.Posts     routeUserPostsGet
      resource config.Path.Users.Threads   routeUserThreads
      resource config.Path.Posts           routeForPost
      resource config.Path.Threads         routeThread
      resource config.Path.Cats.Root       routeCategoryMain
      resource config.Path.Cats.Category   routeCategory
      resource config.Path.Lost            routeLost
    }
