module Clie.Messages

open System

type UserAuth =
  { Username : string
    Token    : string }

type MenuMessages =
  | MenuLogin
  | MenuLogout

type LoginMessages =
  | TokenGet    of string
  | SetUsername of string
  | SetPassword of string
  | AuthError   of exn
  | ClickLogin

type CategoryMessages =
  | CategoryGet  of string
  | CatNotFound  of exn
  | ClickCatLink

type ShownCatMessages =
  | ShownCatGet      of string
  | ShownCatNotFound of exn

type ThreadMessages =
  | ThreadGet       of string
  | PostGet         of string
  | NotFound        of exn
  | SetTitleReply   of string
  | SetContentReply of string
  | ClickReply

type PostMessages =
  | PostGet of string
  | NotFound of exn

type UserMessages =
  | UserGet       of string
  | NotFound      of exn
  | UserPostGet   of string
  | UserThreadget of string

type SignUpMessages =
  | SetUsername of string
  | SetPassword of string
  | SignedUp    of string
  | ClickSignUp
  | BlockedLOL  of exn

type AppMessage =
  | Open
  | OpenSignUp
  | SignUpMsg   of SignUpMessages
  | OpenLogin
  | LoginMsg    of LoginMessages
  | LoggedIn
  | UserMsg     of UserMessages
  | PostMsg     of PostMessages
  | ThreadMsg   of ThreadMessages
  | CategoryMsg of CategoryMessages
  | ShownCatMsg of ShownCatMessages
  | Logout
  | LoggedOut
  | ShitHitsTheFan of exn
