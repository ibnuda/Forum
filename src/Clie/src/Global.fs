module Clie.Global

open System

type Page =
  | SignUp
  | UserInfo    of string
  | Main
  | Login
  | CategoryId  of string
  | Category
  | Thread      of string
  | SinglePost  of string

let toHash =
  function
  | SignUp       -> "#signup"
  | Login        -> "#login"
  | UserInfo str -> sprintf "#user/%s" str
  | Main         -> "#"
  | Category     -> "#category"
  | CategoryId s -> sprintf "#category/%s" s
  | Thread str   -> sprintf "#thread/%s" str
  | SinglePost str     -> sprintf "#post/%s" str

type UserRequest =
  { username : string
    password : string }
  static member Create (u, p) =
    { username = u
      password = p }
  static member Empty =
    UserRequest.Create ("", "")
let setuserreq name req =
  { req with username = name }
let setpassreq pass req =
  { req with password = pass }

type Qualification =
  { id    : int
    value : int
    name  : string }
  static member Create (i, v, n) =
    { id    = i
      value = v
      name  =  n }
  static member Empty =
    Qualification.Create (-3, -3, "Non Existent")
type UserInformation =
  { id       : string
    username : string
    qualification : Qualification }
  static member Create (i, u, q) =
    { id       = i
      username = u
      qualification = q }
  static member Empty =
    UserInformation.Create (Guid.Empty.ToString (), "Non Existent User", Qualification.Empty)

type Activity =
  { id    : int
    lock  : bool
    at    : DateTime
    by    : Guid }
  static member Create (id, lock, at, by) =
    { id    = id
      lock  = lock
      at    = at
      by    = by }
  static member Empty =
    Activity.Create (0, true, DateTime.MinValue, Guid.Empty)
