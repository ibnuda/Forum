namespace Forum

open Unif.ForumModels

module BusinessDefaults =

  let admin       = Qualification.Create ( 4,  4, "Administrator")
  let senmod      = Qualification.Create ( 3,  3, "Senior Moderator")
  let moderator   = Qualification.Create ( 2,  2, "Moderator")
  let normaluser  = Qualification.Create ( 1,  1, "User")
  let banned      = Qualification.Create (-1, -1, "Banned")

  let internal stdLockT at by = Activity.Create (0, true,  at, by)
  let internal stdOpenT at by = Activity.Create (1, false, at, by)
