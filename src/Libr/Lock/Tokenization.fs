namespace Forum

open System.IO
open System.Security.Cryptography

open Chessie.ErrorHandling
open Chiron
open Jose

open Unif.Errors
open Unif.Auth
open Unif

module Tokenization =

  open Utilities

  let private jsonToTokenWithROP json =
    json
    |> jsonToModel Token.decode

  let tokenToJson = modelToJson Token.encode

  let private createPass () =
    let cryp = RandomNumberGenerator.Create ()
    let rand = Array.init 32 byte
    cryp.GetBytes rand
    rand

  let private defaultPass = Array.init 32 byte

  let private pass () =
    try
      let fin = FileInfo "../../SecretToken.txt"
      if not fin.Exists then
        let passp = createPass ()
        if not fin.Directory.Exists then
          fin.Directory.Create ()
        File.WriteAllBytes (fin.FullName, passp)
      File.ReadAllBytes fin.FullName
    with
    | :? IOException -> defaultPass

  let private a2 = JweAlgorithm.A256KW
  let private hs = JweEncryption.A256CBC_HS512

  let private encodeJWT payload () =
    JWT.Encode (payload, pass (), a2, hs) |> ok
  let private decodeJWT token () =
    JWT.Decode (token, pass (), a2, hs) |> ok

  let decodeJWTYOLO token =
    match
      JWT.Decode (Option.get token, pass (), a2, hs)
      |> Json.parse
      |> JsonResult.bind (Json.Decode.jsonObjectWith Token.decode) with
    | JPass token -> token
    | JFail _     -> failwith "FML"

  let userToJWT expiredIn user =
    let userToTokenJson (user : ForumModels.ForumUser) expiredin =
      Token.Create (user.Qualification , user.Id, expiredin)
      |> tokenToJson
    encodeJWT (userToTokenJson user expiredIn) |> runWithException

  let jwtToToken jwt =
    jwt
    |>  decodeJWT
    |>  runWithException
    >>= jsonToTokenWithROP

  let getToken jwtOpt =
    failIfNone Unauthorized jwtOpt >>= jwtToToken
