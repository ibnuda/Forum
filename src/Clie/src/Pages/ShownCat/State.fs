module Clie.Pages.ShownCat.State

open System

open Fable.Core
open Fable.Core.JsInterop
open Fable.Import

open Fable.PowerPack
open Fable.PowerPack.Fetch.Fetch_types

open Elmish

open Clie.Utility.Utility
open Clie.Messages

open Types

let private get cid = getItem "categories" cid

let private getCat cid =
  Cmd.ofPromise get cid ShownCatGet ShownCatNotFound

let init cid : ShownCatModel * Cmd<ShownCatMessages> =
  ShownCatModel.Empty,
  getCat cid

let update (message : ShownCatMessages) (model : ShownCatModel) =
  match message with
  | ShownCatGet something ->
    setcateg (ofJson<ShownCategory> something) model,
    Cmd.none
  | ShownCatNotFound ex ->
    seterror ex.Message model,
    Cmd.none
