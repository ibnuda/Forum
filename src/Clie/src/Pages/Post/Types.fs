module Clie.Pages.Post.Types

open System

open Clie.Global

type Post =
  { id      : string
    author  : UserInformation
    created : DateTime
    title   : string option
    content : string }
  static member Empty =
    { id      = ""
      author  = UserInformation.Empty
      created = DateTime.MinValue
      title   = None
      content = "Failed to load."}

type PostDataModel =
  { ErrorMes : string
    PostData : Post }
  static member Empty =
    { ErrorMes = ""
      PostData = Post.Empty }

let seterr err model =
  { model with ErrorMes = err }

let setpos pos model =
  { model with PostData = pos }
