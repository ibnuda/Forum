module Clie.Utility.Utility

open System

open Fable.Core.JsInterop
open Fable.PowerPack
open Fable.PowerPack.Fetch
open Fable.PowerPack.Fetch.Fetch_types
open Fable.Import

open Clie.Messages

let load<'T> key =
  Browser.localStorage.getItem key
  |> unbox
  |> Option.map (JS.JSON.parse >> unbox<'T>)

let save key (data : 'T) =
  Browser.localStorage.setItem (key, JS.JSON.stringify data)

let delete key =
  Browser.localStorage.removeItem key

let sendReq (req : 'T) token url =
  promise {
    let body = toJson req

    let properties = [
      RequestProperties.Method HttpMethod.POST
      requestHeaders [
        HttpRequestHeaders.Authorization token
      ]
      RequestProperties.Body (unbox body)
    ]

    try
      let! response = Fetch.fetch url properties
      match response.Ok with
      | true ->
        let! data = response.text ()
        return data
      | false ->
        return! failwithf "Err: %s" response.StatusText
    with
    | _ -> return! failwith "How what the fuck!"
  }

let getItem (kind : string) (itemid : string option) =
  promise {
    let itemid =
      match itemid with
      | Some item -> "/" + item
      | None -> ""
    let url = "http://localhost:5000/" + kind + itemid
    let properties = [
      RequestProperties.Method HttpMethod.GET
      requestHeaders [
        HttpRequestHeaders.Accept "application/json"
      ]
    ]
    try
      let! response = fetch url properties
      match response.Ok with
      | true ->
        return! response.text()
      | false ->
        return! failwithf "GET err: %s" response.StatusText
    with
    | _ -> return! failwith "Can't do that, m8."
  }
