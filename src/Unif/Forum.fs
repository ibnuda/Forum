namespace Unif

open System

open Aether
open Chiron
open Operators

module ForumModels =

  type QualifValue =
    | Jailed
    | Banned
    | Normal
    | ModeraTroll
    | SeniorTroll
    | AdministraTroll
    static member ToInt =
      function
      | Jailed -> -1
      | Banned -> 0
      | Normal -> 1
      | ModeraTroll -> 2
      | SeniorTroll -> 3
      | AdministraTroll -> 4
    static member FromInt =
      function
      | 4 -> AdministraTroll
      | 3 -> SeniorTroll
      | 2 -> ModeraTroll
      | 1 -> Normal
      | 0 -> Banned
      | _ -> Jailed

  type Qualification =
    { Id    : int
      Value : int
      Name  : string }
    static member Create (id, value, name) =
      { Id    = id
        Value = value
        Name  = name }
    static member V =
      (fun q    -> q.Value),
      (fun v q  -> { q with Value = v })
    static member N =
      (fun q    -> q.Name),
      (fun n q  -> { q with Name = n })

  type Activity =
    { Id    : int
      Lock  : bool
      At    : DateTime
      By    : Guid }
    static member Create (id, lock, at, by) =
      { Id    = id
        Lock  = lock
        At    = at
        By    = by }
    static member Empty =
      Activity.Create (0, false, DateTime.MinValue, Guid.Empty)
    static member L =
      (fun a    -> a.Lock),
      (fun l a  -> { a with Lock = l })
    static member B =
      (fun a    -> a.By),
      (fun b a  -> { a with By = b })

  type ForumUser =
    { Id            : Guid
      Username      : string
      Password      : string
      Qualification : Qualification }
    static member Create (id, username, password, qualification) =
      { Id            = id
        Username      = username
        Password      = password
        Qualification = qualification }
    static member Empty =
      ForumUser.Create (Guid.Empty, "EMPTY", "", Qualification.Create(-3, -3, "System"))
    static member P =
      (fun user       -> user.Password),
      (fun pass user  -> { user with Password = pass })
    static member Q =
      (fun user               -> user.Qualification),
      (fun qualification user -> { user with Qualification = qualification })

  type ForumCategory =
    { Id               : Guid
      Name             : string
      Locked           : bool
      Description      : string
      Moderatrolls     : Guid list
      MinimalRequirementToParticipate : int }
    static member Create (id, name, locked, desc, mods, minReq) =
      { Id               = id
        Name             = name
        Locked           = locked
        Description      = desc
        Moderatrolls     = mods
        MinimalRequirementToParticipate = minReq }
    static member N =
      (fun cat                        -> cat.Name),
      (fun name (cat : ForumCategory) -> { cat with Name = name })
    static member D =
      (fun cat      -> cat.Description),
      (fun desc cat -> { cat with Description = desc })
    static member L =
      (fun cat        -> cat.Locked),
      (fun locked cat -> { cat with Locked = locked })
    static member Ms =
      (fun cat -> cat.Moderatrolls),
      (fun mods cat -> { cat with Moderatrolls = mods })
    static member MRP =
      (fun cat -> cat.MinimalRequirementToParticipate),
      (fun minReq cat -> { cat with MinimalRequirementToParticipate = minReq })

  type ForumPost =
    { Id      : Guid
      Author  : Guid
      Created : DateTime
      Title   : string option
      Content : string }
    static member Create (id, author, created, title, content) =
      { Id      = id
        Author  = author
        Created = created
        Title   = title
        Content = content }
    static member Empty =
      ForumPost.Create (Guid.Empty, Guid.Empty, DateTime.MinValue, None, "-")
    static member T =
      (fun post       -> post.Title),
      (fun title post -> { post with Title = title })
    static member C =
      (fun post         -> post.Content),
      (fun content post -> { post with Content = content })
    static member Cr =
      (fun post         -> post.Created),
      (fun created post -> { post with Created = created })

  type ForumThread =
    { Id          : Guid
      CategoryId  : Guid
      Title       : string
      Author      : Guid
      Activity    : Activity
      LastActive  : DateTime
      LastBy      : Guid
      Replies     : Guid list }
    static member Create (id, catId, title, author, activity, last, by, postIds ) =
      { Id          = id
        CategoryId  = catId
        Title       = title
        Author      = author
        Activity    = activity
        LastActive  = last
        LastBy      = by
        Replies     = postIds }
    static member Empty =
      ForumThread.Create (Guid.Empty, Guid.Empty, "-", Guid.Empty, Activity.Empty, DateTime.MinValue, Guid.Empty, [])
    static member CI =
      (fun thread       -> thread.CategoryId),
      (fun catId thread -> { thread with CategoryId = catId })
    static member T =
      (fun thread                       -> thread.Title),
      (fun title (thread : ForumThread) -> { thread with Title = title })
    static member Au =
      (fun thread                         -> thread.Author),
      (fun author (thread : ForumThread)  -> { thread with Author = author })
    static member La =
      (fun thread             -> thread.LastActive),
      (fun lastActive thread  -> { thread with LastActive = lastActive })
    static member Lb =
      (fun thread         -> thread.LastBy),
      (fun lastBy thread  -> { thread with LastBy = lastBy })
    static member Ac =
      (fun thread           -> thread.Activity),
      (fun activity thread  -> { thread with Activity = activity })
    static member Rs =
      (fun thread         -> thread.Replies),
      (fun replies thread -> { thread with Replies = replies })

  let getQualifValue            = Optic.get Qualification.V
  let getQualifName             = Optic.get Qualification.N
  let getActivityLock           = Optic.get Activity.L
  let getActivityBy             = Optic.get Activity.B
  let getUserPassword           = Optic.get ForumUser.P
  let getUserQualification      = Optic.get ForumUser.Q
  let getUserQualificationValue = Compose.lens ForumUser.Q  Qualification.V |> Optic.get
  let getUserQualificationName  = Compose.lens ForumUser.Q Qualification.N |> Optic.get
  let getPostCreated            = Optic.get ForumPost.Cr
  let getPostTitle              = Optic.get ForumPost.T
  let getPostContent            = Optic.get ForumPost.C
  let getThreadCatId            = Optic.get ForumThread.CI
  let getThreadTitle            = Optic.get ForumThread.T
  let getThreadActivity         = Optic.get ForumThread.Ac
  let getThreadActivityLock     = Compose.lens ForumThread.Ac Activity.L |> Optic.get
  let getThreadActivityBy       = Compose.lens ForumThread.Ac Activity.B |> Optic.get
  let getThreadLastActive       = Optic.get ForumThread.La
  let getThreadLastBy           = Optic.get ForumThread.Lb
  let getThreadReplies          = Optic.get ForumThread.Rs
  let getCategoryName           = Optic.get ForumCategory.N
  let getCategoryDesc           = Optic.get ForumCategory.D
  let getCategoryLocked         = Optic.get ForumCategory.L
  let getCategoryModerators     = Optic.get ForumCategory.Ms
  let getCategoryMinReq         = Optic.get ForumCategory.MRP

  let setQualifValue            = Optic.set Qualification.V
  let setQualifName             = Optic.set Qualification.N
  let setActivityLock           = Optic.set Activity.L
  let setActivityBy             = Optic.set Activity.B
  let setUserPassword           = Optic.set ForumUser.P
  let setUserQualification      = Optic.set ForumUser.Q
  let setUserQualificationValue = Compose.lens ForumUser.Q Qualification.V |> Optic.set
  let setUserQualificationName  = Compose.lens ForumUser.Q Qualification.N |> Optic.set
  let setPostCreated            = Optic.set ForumPost.Cr
  let setPostTitle              = Optic.set ForumPost.T
  let setPostContent            = Optic.set ForumPost.C
  let setThreadCatId            = Optic.set ForumThread.CI
  let setThreadTitle            = Optic.set ForumThread.T
  let setThreadActivity         = Optic.set ForumThread.Ac
  let setThreadActivityLock     = Compose.lens ForumThread.Ac Activity.L |> Optic.set
  let setThreadActivityBy       = Compose.lens ForumThread.Ac Activity.B |> Optic.set
  let setThreadLastActive       = Optic.set ForumThread.La
  let setThreadLastBy           = Optic.set ForumThread.Lb
  let setThreadReplies          = Optic.set ForumThread.Rs
  let setCategoryName           = Optic.set ForumCategory.N
  let setCategoryDesc           = Optic.set ForumCategory.D
  let setCategoryLocked         = Optic.set ForumCategory.L
  let setCategoryModerators     = Optic.set ForumCategory.Ms
  let setCategoryMinReq         = Optic.set ForumCategory.MRP

  module Qualification =
    let create id value name = Qualification.Create (id, value, name)
    let encode (qualif : Qualification) jsonObject =
      jsonObject
      |> Json.Encode.required Json.Encode.int    "id"    qualif.Id
      |> Json.Encode.required Json.Encode.int    "value" qualif.Value
      |> Json.Encode.required Json.Encode.string "name"  qualif.Name
    let decode =
      create
      <!> Json.Decode.required Json.Decode.int    "id"
      <*> Json.Decode.required Json.Decode.int    "value"
      <*> Json.Decode.required Json.Decode.string "name"
  module Activity =
    let create id lock at by = Activity.Create (id, lock, at, by)
    let encode (activity : Activity) jsonObject =
      jsonObject
      |> Json.Encode.required Json.Encode.int      "id"   activity.Id
      |> Json.Encode.required Json.Encode.bool     "lock" activity.Lock
      |> Json.Encode.required Json.Encode.dateTime "at"   activity.At
      |> Json.Encode.required Json.Encode.guid     "by"   activity.By
    let decode =
      create
      <!> Json.Decode.required Json.Decode.int      "id"
      <*> Json.Decode.required Json.Decode.bool     "lock"
      <*> Json.Decode.required Json.Decode.dateTime "at"
      <*> Json.Decode.required Json.Decode.guid     "by"

