namespace Forum

open System
open System.IO
open System.Text

open Chessie.ErrorHandling
open Chiron

open Unif.Errors

module Utilities =
  module D = Json.Decode
  module E = Json.Encode

  let flip f x y = f y x

  let nao () = DateTime.Now.ToUniversalTime ()

  let failIfNoneOrEmpty a =
    match a with
    | Some str ->
      if String.IsNullOrWhiteSpace str then
        fail EmptyQuery
      else
        ok str
    | None ->
      fail EmptyQuery

  let sOptToGuid str =
    match str with
    | Some str ->
      match Guid.TryParse str with
      | true, guid -> Some guid
      | false, _   -> None
    | None -> None

  let strToGuid str =
    match Guid.TryParse str with
    | false, _ -> InvalidId str |> fail
    | true, guid -> guid |> ok

  let strToInt str =
    match Int32.TryParse str with
    | false, _ -> InvalidId str |> fail
    | true, inte -> inte |> ok

  let chunkList page perPage oId (lst : 'T list) : Result<'T list, Failures> =
    let page = if page < 1 then 1 else page
    let leng = lst.Length
    if leng < ((page - 1) * perPage) then
      SpecifiedPageNotFound (oId, page) |> fail
    else
      lst
      |> List.chunkBySize perPage
      |> List.skip (page - 1)
      |> List.head
      |> ok

  let runWithException (f : unit -> Result<'T, Failures>) =
    try
      f ()
    with
    | :? ArgumentNullException as x -> fail <| ArgumentNull x
    | :? ArgumentException -> fail Argument
    | :? UnauthorizedAccessException -> fail Unauthorized
    | :? PathTooLongException -> fail PathTooLong
    | :? DirectoryNotFoundException -> fail DirectoryNotFound
    | :? IOException -> fail InputOutput
    | :? NotSupportedException -> fail NotSupported
    | :? NullReferenceException -> fail NullReference
    | :? ObjectDisposedException -> fail ObjectDisposed
    | :? OutOfMemoryException -> fail OutOfMemory
    | :? Security.Cryptography.CryptographicException
    | :? Jose.IntegrityException -> fail Integrity
    | :? Jose.EncryptionException -> fail Encryption
    | :? Jose.InvalidAlgorithmException -> fail InvalidAlgorithm
    | _ -> fail <| TFIsGoingOn

  let jsonToModel (decoder : JsonObject -> JsonResult<'T>) jsonString =
    let parsingResult =
      jsonString
      |> Json.parse
      |> JsonResult.bind (D.jsonObjectWith decoder)
    match parsingResult with
    | JPass result -> result |> ok
    | JFail _ -> NotAValidPayload jsonString |> fail

  let inline modelToJson (encoder : 'T -> JsonObject -> JsonObject) model =
    model
    |> E.jsonObjectWith encoder
    |> Json.format

  let rec askPassword () =
    match Console.ReadLine() with
    | inp when String.IsNullOrEmpty inp ->
      printfn "Please write something."
      askPassword ()
    | password -> password

  let read (stream : Stream) =
    try
      use a = new StreamReader (stream, Encoding.UTF8)
      a.ReadToEnd () |> ok
    with
    | :? ArgumentException     -> Argument |> fail
    | :? OutOfMemoryException  -> OutOfMemory |> fail
    | :? IOException        -> InputOutput |> fail
