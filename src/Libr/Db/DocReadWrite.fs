namespace Forum

open Marten

open Utilities
open Unif.ForumModels

module DocReadWrite =

  open DocHelpers

  [<RequireQualifiedAccessAttribute>]
  module Read =

    let load<'a> guid =
      use ses = store.LightweightSession ()
      Session.loadByGuid<'a> guid ses

  [<RequireQualifiedAccessAttribute>]
  module Write =

    let writeEntity entity =
      use ses = store.LightweightSession ()
      Session.storeSingle entity ses
      Session.saveChanges ses
      entity

    let writeEntities entities =
      use ses = store.LightweightSession ()
      Session.storeMany entities ses
      Session.saveChanges ses
      entities

    let deleteEntity entity =
      use ses = store.LightweightSession ()
      Session.deleteEntity entity ses
      Session.saveChanges ses

    let deleteEntities entities =
      use ses = store.LightweightSession ()
      entities
      |> Seq.iter (flip Session.deleteEntity ses)
      Session.saveChanges ses

  [<RequireQualifiedAccessAttribute>]
  module Exist =

    let user uid =
      use session = store.LightweightSession ()
      session
      |> Session.query<ForumUser>
      |> Queryable.filter <@ fun u -> u.Id = uid @>
      |> Queryable.tryExactlyOne
      |> Option.isSome

    let post uid =
      use session = store.LightweightSession ()
      session
      |> Session.query<ForumPost>
      |> Queryable.filter <@ fun u -> u.Id = uid @>
      |> Queryable.tryExactlyOne
      |> Option.isSome

    let thread uid =
      use session = store.LightweightSession ()
      session
      |> Session.query<ForumThread>
      |> Queryable.filter <@ fun u -> u.Id = uid @>
      |> Queryable.tryExactlyOne
      |> Option.isSome

    let category uid =
      use session = store.LightweightSession ()
      session
      |> Session.query<ForumCategory>
      |> Queryable.filter <@ fun u -> u.Id = uid @>
      |> Queryable.tryExactlyOne
      |> Option.isSome
