module Clie.Pages.Login.Types

open System

open Clie.Global

type JWT =  string

type LoginState =
  | LoggedInState of JWT
  | LoggedOutState

type LoginModel =
  { State    : LoginState
    LoginReq : UserRequest
    ErrMess  : string }
  static member Empty =
    { State    = LoggedOutState
      LoginReq = UserRequest.Empty
      ErrMess  = "" }

let setname name model =
  { model with LoginReq = setuserreq name model.LoginReq }

let setpass pass model =
  { model with LoginReq = setpassreq pass model.LoginReq }

let setstat sign model =
  { model with State = sign }

let setmess mess model =
  { model with ErrMess = mess }
