namespace Forum

open System

open Chessie.ErrorHandling
open Marten

open Unif.Errors
open Unif.ForumModels
open Unif.MainModels

module ServeUser =

  open BusinessDefaults
  open DocHelpers
  open DocReadWrite
  open ManageUser

  let getShownUser userId =
    Read.load<ForumUser> userId
    |> failIfNone (NonExistentUser userId)
    |> lift ShownUser.CreateFromForumUser

  let getUserByGuidString (str : string) =
    match Guid.TryParse str with
    | false, _  -> InvalidId str |> fail
    | true, gid ->
      getShownUser gid

  let getUsersPosts (user : ShownUser) =
    use session = store.LightweightSession ()
    session
    |> Session.query<ForumPost>
    |> Queryable.filter <@ fun p -> p.Author = user.Id @>
    |> Queryable.orderByDescending <@ fun p -> p.Created @>
    |> Seq.map ListedPost.CreateFromPost
    |> Seq.toList

  let getUsersThreads (user : ShownUser) =
    use session = store.LightweightSession ()
    session
    |> Session.query<ForumThread>
    |> Queryable.filter <@ fun t -> t.Author = user.Id @>
    |> Queryable.orderByDescending <@ fun t -> t.LastActive @>
    |> Seq.map ListedThreadCompact.CreateFromForumThread
    |> Seq.toList

  let getUserName userId =
    use session = store.LightweightSession ()
    let user =
      session
      |> Session.query<ForumUser>
      |> Queryable.filter <@ fun u -> u.Id = userId @>
      |> Queryable.tryExactlyOne
    match user with
    | Some user -> user.Username
    | None      -> "Empty"

  let getUserByName username =
    let user = checkByUsername username
    user
    |> failIfNone (NonExistentUsername username)
    |> lift (ShownUser.CreateFromForumUser)

  let private getQualifiedUser qualif =
    use session = store.LightweightSession ()
    session
    |> Session.query<ForumUser>
    |> Queryable.filter <@ fun u -> u.Qualification.Value = qualif.Value @>
    |> Queryable.orderBy <@ fun user -> user.Username @>
    |> Seq.toList

  let getMods ()    = getQualifiedUser moderator
  let getSenMods () = getQualifiedUser senmod
