namespace Serv

open Freya.Core
open Operators
open Freya.Optics.Http
open Freya.Routers.Uri.Template

module ReqHandlRes =

  open Unif.ResponseModels

  open ProcessRes

  [<RequireQualifiedAccessAttribute>]
  module Util =

    open Freya.Types

    open Unif.Errors

    open Forum.Utilities
    open Forum.Locking
    open Forum.Tokenization
    open Forum.DocReadWrite

    let private header =
      Request.header_ >> Freya.Optic.get

    let item =
      Route.atom_ >> Freya.Optic.get

    let authHeader = header "Authorization"

    let isAuth =
      authHeader >>= (Lock.authorize >> Freya.init)

    let token =
      authHeader >>= (decodeJWTYOLO >> Freya.init)

    let payload =
      Freya.Optic.get Request.body_

    let minPut minReq =
      freya {
        let! meth = Freya.Optic.get Request.method_
        return!
          match meth with
          | Http.Method.CONNECT  -> Freya.init false
          | Http.Method.Custom _ -> Freya.init false
          | Http.Method.GET      -> Freya.init true
          | Http.Method.DELETE   -> Freya.init false
          | Http.Method.POST     -> isAuth
          | Http.Method.PUT      -> authHeader
                                    >>= (Lock.isQualified minReq >> Freya.init)
          | Http.Method.HEAD     -> Freya.init false
          | _                    -> Freya.init false
      }

    let minCreate minReq =
      Freya.Optic.get Request.method_ >>= fun a ->
        match a with
        | Http.Method.POST -> authHeader >>= (Lock.isQualified minReq >> Freya.init)
        | _                -> Freya.init false

    let private exists f =
      item "itemid" >>= fun item ->
        match item |> sOptToGuid |> Option.map f with
        | Some re -> re
        | None    -> false
        |> Freya.init

    let isCreated (res : Freya<Chessie.ErrorHandling.Result<ForumResponse, Failures>>) =
      res >>= (Chessie.ErrorHandling.Trial.failed >> not >> Freya.init)

    let userExists = exists Exist.user
    let postExists = exists Exist.post
    let threExists = exists Exist.thread
    let cateExists = exists Exist.category

  let tokenCreateH =
    authCreateReq
    <!> Util.payload
    >>= Freya.fromAsync

  let catCreateH =
    catCreateReq
    <!> Util.token
    <*> Util.payload
    >>= Freya.fromAsync

  let catGetH =
    catGetReq
    <!> Util.item "itemid"
    <*> Util.item "page"
    >>= Freya.fromAsync

  let catListH =
    catMainReq
    <!> Freya.init ()
    >>= Freya.fromAsync

  let changeUserQH =
    changeUserQualifReq
    <!> Util.token
    <*> Util.item "itemid"
    <*> Util.payload
    >>= Freya.fromAsync

  let gembokCatH =
    gembokCatCokReq
    <!> Util.token
    <*> Util.item "itemid"
    <*> Util.payload
    >>= Freya.fromAsync

  let gembokThrH =
    gembokThreadReq
    <!> Util.token
    <*> Util.item "itemid"
    <*> Util.payload
    >>= Freya.fromAsync

  let postEditH =
    postEditReq
    <!> Util.token
    <*> Util.item "itemid"
    <*> Util.payload
    >>= Freya.fromAsync

  let postGetH =
    postGetReq
    <!> Util.item "itemid"
    >>= Freya.fromAsync

  let thrGetH =
    threadGetReq
    <!> Util.item "itemid"
    <*> Util.item "page"
    >>= Freya.fromAsync

  let thrCreateH =
    threadCreateReq
    <!> Util.token
    <*> Util.item "itemid"
    <*> Util.payload
    >>= Freya.fromAsync

  let thrReplyH =
    threadReplyReq
    <!> Util.token
    <*> Util.item "itemid"
    <*> Util.payload
    >>= Freya.fromAsync

  let userCreateH =
    userCreateReq
    <!> Util.payload
    >>= Freya.fromAsync

  let userGetH =
    userDataGetReq
    <!> Util.item "itemid"
    >>= Freya.fromAsync

  let userPostGetH =
    userPostsGetReq
    <!> Util.item "itemid"
    >>= Freya.fromAsync

  let userThreGetH =
    userThreadsGetReq
    <!> Util.item "itemid"
    >>= Freya.fromAsync
