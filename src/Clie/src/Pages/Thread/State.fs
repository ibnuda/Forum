module Clie.Pages.Thread.State

open System

open Fable.Core
open Fable.Core.JsInterop
open Fable.Import

open Fable.PowerPack
open Fable.PowerPack.Fetch.Fetch_types

open Elmish

open Clie.Utility.Utility
open Clie.Messages
open Clie.Pages.Post.Types

open Types

let getP pid =
  let getpost = getItem "posts"
  Cmd.ofPromise getpost pid ThreadMessages.PostGet ThreadMessages.NotFound

let getT tid =
  let getthread = getItem "threads"
  Cmd.ofPromise getthread tid ThreadMessages.ThreadGet ThreadMessages.NotFound

let init tid : ThreadModel * Cmd<ThreadMessages> =
  ThreadModel.Empty,
  getT tid

let update (message : ThreadMessages) (model : ThreadModel) =
  match message with
  | ThreadMessages.ThreadGet threadjson ->
    let thr = ofJson<Thread> threadjson
    match thr.replies with
    | x :: xs ->
      model
      |> setthread thr
      |> deletereply x,
      getP <| Some x
    | [ ] ->
      model,
      [ ]
  | ThreadMessages.PostGet postjson ->
    let pos = ofJson<Post> postjson
    match model.ThreadData.replies with
    | x :: xs ->
      schwampthing pos x model,
      getP <| Some x
    | [ ] ->
      model
      |> setpostappend pos,
      [ ]
  | ThreadMessages.NotFound ex ->
    model
    |> seterror ex.Message,
    [ ]
  | ThreadMessages.SetTitleReply title ->
    failwith "Not Implemented"
  | ThreadMessages.SetContentReply content ->
    failwith "Not Implemented"
  | ThreadMessages.ClickReply ->
    failwith "Not Implemented"
