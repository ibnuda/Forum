module TokenizationTests

open System
open Xunit

open Chessie.ErrorHandling

open Unif.Errors
open Unif.ForumModels
open Unif.Auth

open Forum.Utilities
open Forum.ForumModify
open Forum.Tokenization

[<Fact>]
let EncodeDecode () =
  let t = nao () + TimeSpan.FromHours 1.
  let qualif = Qualification.Create (1, 1, "User")
  let id = Guid.NewGuid ()
  let token = Token.Create (qualif, id, t)
  let aUser = ForumUser.Create (id, "Username", "Password", qualif)
  let jwt = userToJWT t aUser
  let wat =
    jwt >>= jwtToToken
  Assert.Equal (ok token, wat)
