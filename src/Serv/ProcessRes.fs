namespace Serv

open System

open Chessie.ErrorHandling

open Unif.ResponseModels
open Unif.Auth

open Forum.Utilities

module ProcessRes =

  open ApisManage
  open ApisServe
  open ProtocolRes

  let tokenValid () = nao () + TimeSpan.FromMinutes 10.
  let timeout = 20000

  let private processor (mailbox : MailboxProcessor<AnotherInOut>) =
    let reply (channel : AsyncReplyChannel<_>) answer =
      channel.Reply answer
      answer

    let userDataGet chan =
      Serve.User.get >> reply chan

    let userPostsGet chan =
      Serve.User.getPosts >> reply chan

    let userThreadsGet chan =
      Serve.User.getThreads >> reply chan

    let postGet chan =
      Serve.Post.get >> reply chan

    let threadGetA chan tid popt =
      match popt with
      | None      -> Serve.Thread.get tid
      | Some page -> Serve.Thread.getChunk 30 tid page
      |> reply chan

    let catGet chan cid popt =
      match popt with
      | None      -> Serve.Category.get cid
      | Some page -> Serve.Category.getChunk 30 cid page
      |> reply chan

    // Make sure what kind of answer do I really want to serve.
    //let imageGet chan =
    //  Serve.Image.get >> reply chan

    let authCreate chan =
      Manage.Auth.create (tokenValid ()) >> reply chan

    let userCreate chan =
      Manage.User.create (Guid.NewGuid ()) >> reply chan

    let threadCreate chan (auth : Token) cId =
      Manage.Thread.create cId (Guid.NewGuid ()) (Guid.NewGuid ()) auth.UserId (nao ())
      >> reply chan

    let threadReply chan (auth : Token) thrId =
      Manage.Thread.reply thrId (Guid.NewGuid ()) auth.UserId (nao ())
      >> reply chan

    // Use pattern matching to avoid Operators conflict from Freya and Chessie.
    let postEdit chan (auth : Token) pid stream =
      match Manage.Post.edit auth.UserId pid stream with
      | Ok (res, _) -> Serve.Post.get (Some <| res.Id.ToString ())
      | Bad errs    -> errs.Head |> fail
      |> reply chan

    let gembokCatCok chan (auth : Token) tid =
      Manage.Category.gembok auth.UserId tid >> reply chan

    let gembokThreadCok chan (auth : Token) tid =
      Manage.Thread.gembok auth.UserId tid >> reply chan

    let changeUserQualif chan (auth : Token) tid =
      Manage.User.changeQualif auth.UserId tid >> reply chan

    let catMain chan =
      Serve.Category.getMain () |> reply chan

    let catCreate chan (auth : Token) =
      Manage.Category.create auth.UserId (Guid.NewGuid ()) >> reply chan

    let rec loop _ =
      async {
        let! received = mailbox.Receive ()
        return!
          match received with
          | AuthCreate (chan, pl)                -> loop (authCreate chan pl)
          | UserCreate (chan, pl)                -> loop (userCreate chan pl)
          | UserDataGet (chan, uId)              -> loop (userDataGet chan uId)
          | UserPostsGet (chan, uId)             -> loop (userPostsGet chan uId)
          | UserThreadsGet (chan, uId)           -> loop (userThreadsGet chan uId)
          | PostGet (chan, pId)                  -> loop (postGet chan pId)
          | PostEdit (chan, tokn, pid, pl)       -> loop (postEdit chan tokn pid pl)
          | ThreadCreate (chan, tokn, cId, pl)   -> loop (threadCreate chan tokn cId pl)
          | ThreadGet (chan, tId, pg)            -> loop (threadGetA chan tId pg)
          | ThreadReply (chan, tokn, tId, pl)    -> loop (threadReply chan tokn tId pl)
          | ChangeUserQualif(chan, tok, tId, pl) -> loop (changeUserQualif chan tok tId pl)
          | ImageGet _                           -> failwith "Not Implemented"
          | ImageCreate _                        -> failwith "Not Implemented"
          | GembokThreadCok (chan, tok, tid, pl) -> loop (gembokThreadCok chan tok tid pl)
          | GembokCatCok (chan, tok, cid, pl)    -> loop (gembokCatCok chan tok cid pl)
          | CatMain chan                         -> loop (catMain chan)
          | CatGet (chan, cId, page)             -> loop (catGet chan cId page)
          | CatCreate (chan, tokn, pl)           -> loop (catCreate chan tokn pl)
      }

    Empty ""
    |> ok
    |> loop

  let private serverState = MailboxProcessor.Start processor

  let authCreateReq payload       = serverState.PostAndAsyncReply ((fun c -> AuthCreate (c, payload)), timeout)
  let userCreateReq payload       = serverState.PostAndAsyncReply ((fun c -> UserCreate (c, payload)), timeout)
  let userDataGetReq uid          = serverState.PostAndAsyncReply ((fun c -> UserDataGet (c, uid)), timeout)
  let userPostsGetReq uid         = serverState.PostAndAsyncReply ((fun c -> UserPostsGet (c, uid)), timeout)
  let userThreadsGetReq uid       = serverState.PostAndAsyncReply ((fun c -> UserThreadsGet (c, uid)), timeout)
  let changeUserQualifReq t tid p = serverState.PostAndAsyncReply ((fun c -> ChangeUserQualif (c, t, tid, p)), timeout)
  let postGetReq pid              = serverState.PostAndAsyncReply ((fun c -> PostGet (c, pid)), timeout)
  let postEditReq tokn pid pl     = serverState.PostAndAsyncReply ((fun c -> PostEdit (c, tokn, pid, pl)), timeout)
  let threadCreateReq tokn cid pl = serverState.PostAndAsyncReply ((fun c -> ThreadCreate (c, tokn, cid, pl)), timeout)
  let threadGetReq tid pg         = serverState.PostAndAsyncReply ((fun c -> ThreadGet (c, tid, pg)), timeout)
  let threadReplyReq tokn tid pl  = serverState.PostAndAsyncReply ((fun c -> ThreadReply (c, tokn, tid, pl)), timeout)
  // let imageGetReq tokn pl      =
  // let imageCreateReq tokn pl   =
  let gembokThreadReq tokn tid pl = serverState.PostAndAsyncReply ((fun c -> GembokThreadCok (c, tokn, tid, pl)), timeout)
  let gembokCatCokReq tokn tid pl = serverState.PostAndAsyncReply ((fun c -> GembokCatCok (c, tokn, tid, pl)), timeout)
  let catCreateReq tokn pl        = serverState.PostAndAsyncReply ((fun c -> CatCreate (c, tokn, pl)), timeout)
  let catMainReq ()               = serverState.PostAndAsyncReply (CatMain, timeout)
  let catGetReq cid page          = serverState.PostAndAsyncReply ((fun c -> CatGet (c, cid, page)), timeout)

  // WITHOUT MAILBOX PROC.
  // PROOF OF CONCEPT.
  // let fileReq path : byte [] =
  //   match path with
  //   | Some path ->
  //     match readFromPath path with
  //     | Ok (file, _) -> file
  //     | Bad _ -> [||]
  //   | None -> [||]

