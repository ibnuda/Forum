namespace Forum

open Chessie.ErrorHandling

open Unif.Errors
open Unif.ForumModels

module BusinessRules =

  open ForumModify
  open BusinessDefaults

  // MODERATION

  let makeAdmin grantor newadmin =
    match grantor.Qualification.Value with
    | x when x = admin.Value ->
      setUserQualification admin newadmin |> ok
    | _ -> NotQualifiedToChangeQualification (newadmin.Username, admin.Name) |> fail

  let grantSeniorModeration grantor targetGrant =
    match grantor.Qualification.Value with
    | x when x = admin.Value ->
      setUserQualification senmod targetGrant |> ok
    | _ -> NotQualifiedToChangeQualification (targetGrant.Username, senmod.Name) |> fail

  let grantModeration grantor targetGrant =
    match grantor.Qualification.Value with
    | x when x = admin.Value || x = senmod.Value ->
      setUserQualification moderator targetGrant |> ok
    | _     -> NotQualifiedToChangeQualification (targetGrant.Username, moderator.Name) |> fail

  let depriveSeniorModeration depriver targetDeprive =
    match depriver.Qualification.Value with
    | x when x = admin.Value -> setUserQualification moderator targetDeprive |> ok
    | _ -> NotQualifiedToChangeQualification (targetDeprive.Username, senmod.Name) |> fail

  let depriveModeration depriver targetDeprive =
    match depriver.Qualification.Value with
    | x when x = admin.Value || x = senmod.Value ->
      setUserQualification normaluser targetDeprive |> ok
    | _     -> NotQualifiedToChangeQualification (targetDeprive.Username, moderator.Name) |> fail

  // USER MANAGEMENT
  let createNormalUser id username password =
    createUser id username password normaluser

  let editPasswordUser editor target newpass =
    match editor.Qualification.Value, editor.Id,
          target.Qualification.Value, target.Id with
    | eV, _, tV, _ when eV = admin.Value && tV <> banned.Value ->
      editpassword newpass target |> ok
    | eV, eId, _, tId when eId = tId && eV <> banned.Value ->
      editpassword newpass target |> ok
    | _, _, _, _ ->
      NotQualifiedToChangePassword target.Id |> fail

  let banUser executor targetBan =
    if executor = targetBan then
      CannotSelfBan |> fail
    else
      match executor.Qualification.Value, targetBan.Qualification.Value with
      | exec, targ when exec <= admin.Value && exec > normaluser.Value && exec > targ && targ > banned.Value ->
        setUserQualification banned targetBan |> ok
      | exec, targ when exec <= admin.Value && exec > normaluser.Value && exec > targ && targ <= banned.Value ->
        AlreadyBanned targetBan.Id |> fail
      | _ ->
        NotQualifiedToChangeQualification (targetBan.Username, banned.Name) |> fail

  let releaseBan warden targetRelease =
    match warden.Qualification.Value, targetRelease.Qualification.Value with
    | x, y when x >= moderator.Value && x <= admin.Value && y <= banned.Value ->
      setUserQualification normaluser targetRelease |> ok
    | x, y when x >= moderator.Value && x <= admin.Value && y <> banned.Value ->
      CannotUnbanNonBannedUser targetRelease.Id |> fail
    | _ ->
      NotQualifiedToChangeQualification (targetRelease.Username, normaluser.Name) |> fail

  // POST MANAGEMENT
  let createForumPost author id createdAt title content =
    match author.Qualification.Value with
    | x when x < normaluser.Value  -> YouAreBanned |> fail
    | _ ->
      createPost id author.Id createdAt title content |> ok

  let editForumPost editor (targetPost : ForumPost) editTitle editContent =
    match editor.Qualification.Value, targetPost.Author with
    | x, _ when x < normaluser.Value -> YouAreBanned |> fail
    | x, _ when x = admin.Value -> editPost editTitle editContent targetPost |> ok
    | _, x when x = editor.Id ->
      editPost editTitle editContent targetPost |> ok
    | _ -> NotQualifiedToEditPost (targetPost.Id, editor.Id) |> fail

  // THREAD MANAGEMENT
  let initForumThread author threadId (category : ForumCategory) title (forumPost : ForumPost) =
    match author.Qualification.Value,
          forumPost.Author,
          category.MinimalRequirementToParticipate with
    | _, _, _ when category.Locked ->
      CannotCreateThreadInLockedCategory category.Name |> fail
    | qualifValue, _, _ when qualifValue < normaluser.Value  && not category.Locked ->
      YouAreBanned |> fail
    | qualifValue, postAuthId, minReq when qualifValue >= normaluser.Value
                                      && not category.Locked
                                      && postAuthId = author.Id
                                      && minReq <= qualifValue ->
      createThreadFromPost threadId category.Id title forumPost |> ok
    | qualifValue, postAuthId, minReq when qualifValue >= normaluser.Value
                                      && not category.Locked
                                      && postAuthId <> author.Id
                                      && minReq <= qualifValue ->
      NotTheAuthor (author.Id, forumPost.Id) |> fail
    | qualifValue, _, minReq when minReq > qualifValue ->
      NotQualifiedToCreateThreadInCategory (author.Username, category.Name) |> fail
    | qualifValue, _, _ when qualifValue < normaluser.Value ->
      YouAreBanned |> fail
    | _ ->
      TFIsGoingOn |> fail

  let replyForumThread poster (post : ForumPost) targetThread =
    match poster.Qualification.Value, targetThread.Activity, post.Author with
    | x, _, _ when x < normaluser.Value ->
      NotQualifiedToPostInThisForumThread (targetThread.Id, poster.Qualification.Name) |> fail
    | _, locked, _ when locked.Lock -> CannotReplyLockedThread targetThread.Id |> fail
    | _, _, postId when postId = poster.Id -> replyThread post targetThread |> ok
    | _ ->
      PostAuthorAndPosterNotTheSame (post.Author, poster.Id) |> fail

  let lockForumThread at locker targetThread =
    match locker.Qualification.Value, targetThread.Activity with
    | value, activity when not activity.Lock && value > normaluser.Value && value <= admin.Value ->
      setThreadActivity (stdLockT at locker.Id) targetThread |> ok
    | value, activity when activity.Lock && value > normaluser.Value && value <= admin.Value ->
      CannotLockLockedThread targetThread.Id |> fail
    | _ -> NotQualifiedToLockThread (targetThread.Id, locker.Qualification.Name) |> fail

  let unlockForumThread at locker targetThread =
    match locker.Qualification.Value, targetThread.Activity with
    | value, activity when activity.Lock && value > normaluser.Value && value <= admin.Value ->
      setThreadActivity (stdOpenT at locker.Id) targetThread |> ok
    | value, activity when not activity.Lock && value > normaluser.Value && value <= admin.Value ->
      CannotUnlockNotLockedThread targetThread.Id |> fail
    | _ -> NotQualifiedToUnlockThread (targetThread.Id, locker.Qualification.Name) |> fail

  // CATEGORY MANAGEMENT
  let createCategory creator id name desc mods minreq =
    match creator.Qualification.Value with
    | x when x = admin.Value ->
      createCategory id name false desc mods minreq |> ok
    | _ ->
      NotQualifiedToCreateCategory (creator.Id, creator.Qualification.Name) |> fail

  let lockCategory locker category =
    match locker.Qualification.Value, locker.Id, category.Locked with
    | lval, _, islocked when lval = admin.Value && not islocked ->
      lockCategory category |> ok
    | lval, _, islocked when lval = admin.Value && islocked ->
      CategoryAlreadyLocked category.Id |> fail
    | lval, lid, islocked when (lval = senmod.Value || lval = moderator.Value)
                            && isAModInCategory lid category
                            && not islocked ->
      lockCategory category |> ok
    | lval, lid, islocked when (lval = senmod.Value || lval = moderator.Value)
                            && isAModInCategory lid category
                            && islocked ->
      CategoryAlreadyLocked category.Id |> fail
    | _ -> NotQualifiedToLockCategory (locker.Id, category.Name) |> fail

  let unlockCategory locker category =
    match locker.Qualification.Value, locker.Id, category.Locked with
    | lval, _, islocked when lval = admin.Value && islocked ->
      unlockCategory category |> ok
    | lval, _, islocked when lval = admin.Value && not islocked ->
      CategoryAlreadyUnlocked category.Id |> fail
    | lval, lid, islocked when (lval = senmod.Value || lval = moderator.Value)
                            && isAModInCategory lid category
                            && islocked ->
      unlockCategory category |> ok
    | lval, lid, islocked when (lval = senmod.Value || lval = moderator.Value)
                            && isAModInCategory lid category
                            && not islocked ->
      CategoryAlreadyUnlocked category.Id |> fail
    | _ ->
      NotQualifiedToUnlockCategory (locker.Id, category.Name) |> fail

  let addModeratorInCategory (actor : ForumUser) (candidate : ForumUser) (category : ForumCategory) =
    match actor.Qualification.Value, candidate.Qualification.Value, isAModInCategory candidate.Id category with
    | s, x, false when s = admin.Value && x < admin.Value && x > normaluser.Value ->
      addModInCategory candidate category |> ok
    | s, x, false when s = senmod.Value &&  x = moderator.Value ->
      addModInCategory candidate category |> ok
    | s, _, true when s = admin.Value || s = senmod.Value ->
      AlreadyAModInCategory (candidate.Id, category.Name) |> fail
    | _ ->
      NotQualifiedToAddMod actor.Id |> fail

  let removeModeratorInCategory (actor : ForumUser) (moderator : ForumUser) (category : ForumCategory) =
    match actor.Qualification.Value, isAModInCategory moderator.Id category with
    | s, true when s = admin.Value || s = senmod.Value ->
      removeModInCategory moderator category |> ok
    | s, false when s = admin.Value || s = senmod.Value ->
      NotAModInCategory (moderator.Id, category.Name) |> fail
    | _, true ->
      NotQualifiedToChangeQualification (moderator.Username, normaluser.Name) |> fail
    | _, false ->
      Lancang |> fail

  let depriveModerationAndRemoveFromCategories (actor : ForumUser) (moderator : ForumUser) (categories : ForumCategory list) =
    let resRemoveInCats =
      categories
      |> List.filter (isAModInCategory moderator.Id)
      |> List.map (removeModeratorInCategory actor moderator)
      |> collect
    let resDepriveMod = depriveModeration actor moderator
    resRemoveInCats, resDepriveMod
