namespace Serv

open Chessie.ErrorHandling

open Unif.Errors
open Unif.MainModels
open Unif.RequestModels
open Unif.ResponseModels

open Forum.Utilities
open Forum.Locking
open Forum.ManageImage
open Forum.ManageUser
open Forum.ManagePost
open Forum.ManageThread
open Forum.ManageCategory


module ApisManage =

  [<RequireQualifiedAccessAttribute>]
  module Manage =

    [<RequireQualifiedAccessAttribute>]
    module Auth =
      let create expiredIn stream =
        read stream
        >>= flip Lock.createToken expiredIn
        |>  lift NormalString

      let validate time stream =
        read stream
        >>= Lock.validateJwt time

    [<RequireQualifiedAccessAttribute>]
    module User =
      let create guid stream =
        read stream
        >>= jsonToModel UserCreate.decode
        >>= fun uc -> createUser guid uc.Username uc.Password
        |>  lift DataUser

      let changePassword stream =
        read stream
        >>= jsonToModel ChangePass.decode
        >>= fun cp -> changePasswordUser cp.ExecId cp.Target cp.Passwo
        |>  lift ShownUser.CreateFromForumUser
        |>  lift DataUser

      let changeQualif execId uid stream =
        uid
        |>  failIfNoneOrEmpty
        >>= strToGuid
        >>= fun uid ->
          read stream
          >>= jsonToModel Manage.decode
          >>= fun com ->
            match com.Command with
            | GrantAdm      -> makeAdministrator execId uid
            | GrantSenMod   -> makeSenModerator  execId uid
            | GrantMod      -> makeModerator     execId uid
            | DepriveSenMod -> deleteSenMod      execId uid
            | DepriveMod    -> deleteMod         execId uid
            | Ban           -> makeUserBanned    execId uid
            | Release       -> makeUserFree      execId uid
            | _             -> com.Command.ToString () |> InvalidCommand |> fail
            |> lift DataUser

    [<RequireQualifiedAccessAttribute>]
    module Post =
      let create postId authId created stream =
        read stream
        >>= jsonToModel PostObj.decode
        >>= fun pc -> createPost authId postId created pc.Title pc.Content

      let edit editorId pid stream =
        pid
        |>  failIfNoneOrEmpty
        >>= strToGuid
        >>= fun pid ->
          read stream
          >>= jsonToModel PostObj.decode
          >>= fun pe ->
            editPost editorId pid pe.Title pe.Content

    [<RequireQualifiedAccessAttribute>]
    module Thread =
      let create catId threadId postId authId at stream =
        catId
        |> failIfNoneOrEmpty
        >>= strToGuid
        >>= fun cId ->
          Post.create postId authId at stream
          >>= fun p -> initThread p.Author threadId cId p.Title p.Id
          |>  lift (StatusThread.CreateFromForumThread >> DataStatusThread)

      let reply thrId postId authId created stream =
        thrId
        |>  failIfNoneOrEmpty
        >>= strToGuid
        >>= fun tid ->
          read stream
          >>= jsonToModel PostObj.decode
          >>= fun pc ->
            createPost authId postId created pc.Title pc.Content
            >>= fun p -> replyThread authId p.Id tid
            |>  lift (StatusThread.CreateFromForumThread >> DataStatusThread)

      let gembok execId thrId stream =
        thrId
        |>  failIfNoneOrEmpty
        >>= strToGuid
        >>= fun tid ->
          read stream
          >>= jsonToModel Manage.decode
          >>= fun com ->
            match com.Command with
            | LockThread -> lockThread execId tid
            | UnlockThread -> unlockThread execId tid
            | _ -> com.Command.ToString () |> InvalidCommand |> fail
            |> lift (StatusThread.CreateFromForumThread >> DataStatusThread)

    [<RequireQualifiedAccessAttribute>]
    module Category =
      let create creatorId catId stream =
        read stream
        >>= jsonToModel CategoryCreate.decode
        >>= fun cc ->
          initCategory creatorId catId cc.Name cc.Desc cc.Mods cc.MinReq
          |> lift (StatusCategory.CreateFromForumCategory >> DataStatusCategory)

      let gembok execId thrId stream =
        thrId
        |>  failIfNoneOrEmpty
        >>= strToGuid
        >>= fun tid ->
          read stream
          >>= jsonToModel Manage.decode
          >>= fun com ->
            match com.Command with
            | LockCategory -> lockCategory execId tid
            | UnlockCategory -> unlockCategory execId tid
            | _ -> com.Command.ToString () |> InvalidCommand |> fail
            |> lift (StatusCategory.CreateFromForumCategory >> DataStatusCategory)

    [<RequireQualifiedAccessAttribute>]
    module Image =
      let save saverId stream =
        saveImages saverId stream
