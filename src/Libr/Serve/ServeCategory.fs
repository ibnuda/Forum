namespace Forum

open Chessie.ErrorHandling
open Marten

open Unif.Errors
open Unif.ForumModels

module ServeCategory =

  open DocHelpers
  open DocReadWrite
  open ServeCategoryUtil

  let getListedCategory categoryId =
    Read.load<ForumCategory> categoryId
    |> failIfNone (NonExistentCategory categoryId)
    |> lift toListedCategory

  let getShownCategory categoryId =
    Read.load<ForumCategory> categoryId
    |>  failIfNone (NonExistentCategory categoryId)
    >>= toShownCategory

  let getApiShownCategory categoryId =
    Read.load<ForumCategory> categoryId
    |> failIfNone (NonExistentCategory categoryId)
    |> lift toApiShownCategory

  let getChunkedApiShownCategory page perPage categoryId =
    Read.load<ForumCategory> categoryId
    |>  failIfNone (NonExistentCategory categoryId)
    >>= toChunkedApiShownCategory page perPage

  let getChunkedShownCategory page perPage categoryId =
    Read.load<ForumCategory> categoryId
    |>  failIfNone (NonExistentCategory categoryId)
    >>= toChunkedShownCategory page perPage

  let getMainCategories () =
    use session = store.LightweightSession ()
    session
    |> Session.query<ForumCategory>
    |> Seq.map toListedCategory
    |> Seq.toList
