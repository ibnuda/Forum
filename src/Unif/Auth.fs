namespace Unif

open System

open Chiron
open Operators

module Auth =

  open ForumModels

  module D = Json.Decode
  module E = Json.Encode
  module ID = Inference.Json.Decode
  module IE = Inference.Json.Encode

  type UserRequest =
    { Username : string
      Password : string }
    static member Empty =
      { Username = ""
        Password = "" }
    static member Create (uname, pass) : UserRequest =
      { Username = uname
        Password = pass }

  type Token =
    { Quali : Qualification
      UserId : Guid
      ExpiresIn : DateTime }
    static member Create (quali, uid, eIn) =
      { Quali = quali
        UserId = uid
        ExpiresIn = eIn }

  module UserRequest =
    let private create user pass = { Username = user; Password = pass }
    let decode =
      create
      <!> D.required D.string "username"
      <*> D.required D.string "password"

  module Token =
    let private create qualif uid ein = Token.Create (qualif, uid, ein)
    let private eQualif = E.jsonObjectWith Qualification.encode
    let private dQualif = D.jsonObjectWith Qualification.decode
    let encode token jsonObj =
      jsonObj
      |> E.required eQualif    "qualification" token.Quali
      |> E.required E.guid     "userid"        token.UserId
      |> E.required E.dateTime "expiresin"     token.ExpiresIn
    let decode =
      create
      <!> D.required dQualif    "qualification"
      <*> D.required D.guid     "userid"
      <*> D.required D.dateTime "expiresin"
