module Clie.Pages.Menu.Views

open Fable.Core
open Fable.Helpers.React
open Fable.Helpers.React.Props

open Clie.Global
open Clie.Messages
open Clie.Utility.Style
open Fable.Core
open Fable.Helpers.React
open Fable.Helpers.React.Props

open Clie.Global
open Clie.Messages
open Clie.Utility.Style
open Types

let view (model : MenuModel) dispatch =
  nav [ ClassName "navbar" ] [
    div [ ClassName "navbar-brand" ] [
      yield viewLink Page.Main "Main"
    ]
    div [ ClassName "navbar-menu" ] [
      yield viewLink Page.Category "Category"
    ]
    div [ ClassName "navbar-menu navbar-end" ] [
      if model.User = None then
        yield viewLink Page.Login "Login"
        yield viewLink Page.SignUp "Sign Up"
      else
        yield buttonLink (fun _ -> dispatch Logout) [ text [ ] [ str "Logout" ] ]
    ]
  ]
