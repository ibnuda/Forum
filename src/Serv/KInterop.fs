namespace Serv

open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting

open Microsoft.Extensions.Configuration

open Freya.Core
open Operators
open Freya.Polyfills.Kestrel
open Freya.Routers.Uri.Template

module KInterop =

  module ApplicationBuilder =
    let inline useRouteListFromFreya f (app : IApplicationBuilder) =
      let routes                = UriTemplateRouter.Freya f
      let pipeline              = Polyfill.kestrel >?= routes
      let midFunc : OwinMidFunc = OwinMidFunc.ofFreya pipeline
      app.UseOwin (fun p -> p.Invoke midFunc)

    let inline complete (_ : IApplicationBuilder) = ()

  module Config =
    let create ()                              = ConfigurationBuilder ()
    let setBase r (cb : IConfigurationBuilder) = cb.SetBasePath r
    let addJson j (cb : IConfigurationBuilder) = cb.AddJsonFile (j, true, true)
    let addEnvVar (cb : IConfigurationBuilder) = cb.AddEnvironmentVariables ()
    let build (cb : IConfigurationBuilder)     = cb.Build ()

  module WebHost =
    let create ()                         = WebHostBuilder().UseKestrel()
    let contentRoot (root : string)
                    (b : IWebHostBuilder) = b.UseContentRoot root
    let config (config : IConfiguration)
                  (b : IWebHostBuilder)   = b.UseConfiguration config
    let bindTo urls (b : IWebHostBuilder) = b.UseUrls urls
    let configure (f : IApplicationBuilder -> IApplicationBuilder)
                  (b : IWebHostBuilder)   = b.Configure (System.Action<_> (f >> ignore))
    let build (b:IWebHostBuilder)         = b.Build()
    let run (wh:IWebHost)                 = wh.Run()
    let buildAndRun : IWebHostBuilder -> unit = build >> run
