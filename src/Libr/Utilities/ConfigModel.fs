namespace Forum

open Aether
module ConfigModel =

  /// At the moment, there are only two configuration
  /// - DbConfig For database config.
  /// - DIConfig For images.
  /// - TOConfig For timeout and token expiration.
  type Configuration =
    { Db : DbConfig
      DI : DIConfig
      TO : TOConfig }
    static member Create (db, di, ot) =
      { Db = db
        DI = di
        TO = ot }
  /// DbConfig: hostname, port, user, pass, and dbname
  /// for the database that is being used by this prog.
  /// Should be used on `DocHelpers`
  and DbConfig =
    { Host : string
      Port : int
      User : string
      Pass : string
      Name : string }
    static member Create (h, po, u, pa, n) =
      { Host = h
        Port = po
        User = u
        Pass = pa
        Name = n }
  /// DIConfig : directory where the images are being saved
  /// and the name of the empty image (or default 404) for
  /// the requested image.
  and DIConfig =
    { Dir  : string
      Empty: string }
    static member Create (dir, empt) =
      { Dir = dir
        Empty = empt }
  /// TOConfig: request timeout for the server and token
  /// expiration.
  and TOConfig =
    { Request : int
      Expirat : int64 }
    static member Create (req, exp) =
      { Request = req
        Expirat = exp }

module ConfigDec =
  open Chiron
  open Operators

  open ConfigModel

  module D = Json.Decode

  module DbDec =
    let private create h po u pa n = DbConfig.Create (h, po, u, pa, n)
    let decode =
      create
      <!> D.required D.string "host"
      <*> D.required D.int    "port"
      <*> D.required D.string "user"
      <*> D.required D.string "pass"
      <*> D.required D.string "name"

  module DIDec =
    let private create dir empt = DIConfig.Create (dir, empt)
    let decode =
      create
      <!> D.required D.string "dir"
      <*> D.required D.string "empty"

  module TODec =
    let private create req exp = TOConfig.Create (req, exp)
    let decode =
      create
      <!> D.required D.int   "req"
      <*> D.required D.int64 "exp"

  module ConfigurationDec =
    let private create db di ot = Configuration.Create (db, di, ot)
    let private dbdecode = D.jsonObjectWith DbDec.decode
    let private didecode = D.jsonObjectWith DIDec.decode
    let private todecode = D.jsonObjectWith TODec.decode
    let decode =
      create
      <!> D.required dbdecode "db"
      <*> D.required didecode "di"
      <*> D.required todecode "to"
