namespace Forum

module ServeImage =

  open ImagesModels
  open ImagesReadWrite
  open DocReadWrite

  let getImage imageId =
    match Read.load<ImageInDb> imageId with
    | Some image -> image
    | None -> ImageInDb.Empty
    |> readFileFromPath
