module LockTests

open System

open Xunit
open Chessie.ErrorHandling

open Unif.Errors
open Unif.Auth
open Unif.ForumModels

open Forum.Utilities
open Forum.ForumModify
open Forum.BusinessDefaults
open Forum.DocReadWrite
open Forum.LockPreparations
open Forum.Locking


[<Fact>]
let CanLoadRightUser () =
  let expectedUser =
    ForumUser.Create (Guid.NewGuid (), "OwO", "Howie", normaluser)
    |> Write.writeEntity
  let checkedUser = checkUser { Username = "OwO"; Password = "Howie" }
  Write.deleteEntity expectedUser
  Assert.Equal (ok expectedUser, checkedUser)

[<Fact>]
let CannotLoadNotUser () =
  let checkedUser = checkUser { Username = "NotAUser"; Password = "PassUser" }
  Assert.Equal (NonExistentUsername "NotAUser" |> fail, checkedUser)

[<Fact>]
let RoundTripOK () =
  let nowPlus = nao () + TimeSpan.FromMinutes 1.
  let gid = Guid.NewGuid ()
  let expectedToken = Token.Create (normaluser, gid, nowPlus)
  let expectedUser =
    ForumUser.Create (gid, "UwU", "Awooo", normaluser)
    |> Write.writeEntity
  let jwt =
        Lock.createToken """{"username":"UwU","password":"Awooo"}""" nowPlus
    >>= Lock.validateJwt DateTime.MinValue
  Write.deleteEntity expectedUser
  Assert.Equal (expectedToken |> ok, jwt)
