module Clie.Pages.Post.Views

open System

open Elmish

open Fable.Helpers.React
open Fable.Helpers.React.Props

open Clie.Global
open Clie.Messages

open Types

let view (model : PostDataModel) dispatch =
  match String.IsNullOrEmpty model.ErrorMes with
  | false ->
    div [ ] [
      h3 [ ] [
        str model.ErrorMes
      ]
    ]
  | true ->
    div [ ClassName "box" ] [
      article [ ClassName "media" ] [
        div [ ClassName "media-left" ] [
          figure [ ClassName "image is-64x64" ] [
            a [ Href (toHash <| Page.UserInfo model.PostData.author.id) ] [
              img [ Src "/assets/images/128x128.png"
                    Alt model.PostData.author.username ]
            ]
          ]
        ]
        div [ ClassName "media-content" ] [
          div [ ClassName "content" ] [
            p [ ] [
              str model.PostData.content
            ]
          ]
          nav [ ClassName "level" ] [
            div [ ClassName "level-right" ] [
              a [ ClassName "level-item"
                  Href (toHash <| Page.SinglePost model.PostData.id)] [
                span [ ClassName "icon is-small" ] [
                  i [ ClassName "fa fa-link" ] [ ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]
