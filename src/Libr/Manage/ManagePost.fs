namespace Forum

open Chessie.ErrorHandling

open Unif.Errors
open Unif.ForumModels

module ManagePost =

  open BusinessRules
  open DocReadWrite

  let createPost authorId postId createdAt title content =
    match Read.load<ForumUser> authorId with
    | None      -> NonExistentUser authorId |> fail
    | Some user ->
      createForumPost user postId createdAt title content
      |> lift Write.writeEntity

  let editPost editor postId title content =
    match Read.load<ForumUser> editor,
          Read.load<ForumPost> postId with
    | None, _               -> NonExistentUser editor |> fail
    | Some _,    None       -> NonExistentPost postId |> fail
    | Some user, Some post  ->
      editForumPost user post title content
      |> lift Write.writeEntity
