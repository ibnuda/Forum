namespace Forum

open System
open System.IO

open Chessie.ErrorHandling
open HttpMultipartParser

open Unif.Errors

module ImagesReadWrite =

  open Utilities
  open ImagesModels

  // let private dir = config.DI.Dir
  let private dir = "C:\\tools\\forum.serv\\"

  let private saveFilesToPath id up stream =
    let writeToDisk id up (filePart : FilePart) =
      let fileName    = filePart.FileName
      let content     = filePart.Data
      let contentType = filePart.ContentType
      let saved       = up.ToString () + "_" + id.ToString () + "_" + fileName
      let path        = [| dir; (id.ToString () + "_" + fileName) |] |> Path.Combine
      use reader      = new BinaryReader (content)
      use writer      = new FileStream (path, FileMode.CreateNew)
      reader.BaseStream.CopyTo(writer)
      ImageInDb.Create (id, up, fileName, contentType, saved)
    let f id up stream () =
      let parser = MultipartFormDataParser (stream)
      if parser.Files.Count > 0 then
        parser.Files
        |> Seq.map (writeToDisk id up)
        |> ok
      else
        fail EmptyUpload
    f id up stream |> runWithException

  let readFromPath (path : string) =
    let find path () =
      let pathDisk = [| dir; path |] |> Path.Combine
      use stream   = File.Open (pathDisk, FileMode.Open)
      use memstr   = new MemoryStream ()
      stream.CopyTo (memstr)
      memstr.ToArray () |> ok
    find path |> runWithException

  let readFileFromPath (inDb : ImageInDb) =
    let findFile path =
      use stream = File.Open (path, FileMode.Open)
      use memstr = new MemoryStream ()
      stream.CopyTo (memstr)
      memstr.ToArray ()
    let f (inDb : ImageInDb) () =
      let fileRead = findFile inDb.Path
      ImageOutDb.Create (inDb.Id, inDb.Name, fileRead) |> ok
    f inDb |> runWithException

  let saveFiles up stream =
    let gid = Guid.NewGuid ()
    saveFilesToPath gid up stream
