module ServeTests

open System

open Xunit
open Chessie.ErrorHandling

open Unif.Errors
open Unif.ForumModels
open Unif.MainModels

open Forum.BusinessDefaults
open Forum.DocReadWrite
open Forum.ServeUser
open Forum.ServePost
open Forum.ServeThread
open Forum.ServeCategoryUtil

let stdLockT at by = Activity.Create (0, true, at, by)
let stdOpenT at by = Activity.Create (1, false, at, by)

[<Fact>]
let CanLoadByUsers () =
  let adm = ForumUser.Create (Guid.NewGuid (), "AdminUserLoadUsers", "Password", admin)
  let ado = ForumUser.Create (Guid.NewGuid (), "Ngademin", "Password", admin)
  let sen = ForumUser.Create (Guid.NewGuid (), "SeniorUser1", "Password", senmod)
  let anS = ForumUser.Create (Guid.NewGuid (), "SeniorUser2", "Password", senmod)
  let moD = ForumUser.Create (Guid.NewGuid (), "ModUser1", "Password", moderator)
  let anM = ForumUser.Create (Guid.NewGuid (), "ModUser2", "Password", moderator)
  let noA = ForumUser.Create (Guid.NewGuid (), "NormalUserA", "Pass", normaluser)
  let noB = ForumUser.Create (Guid.NewGuid (), "NormalUserB", "Pass", normaluser)
  let noC = ForumUser.Create (Guid.NewGuid (), "NormalUserC", "Pass", normaluser)
  let ban = ForumUser.Create (Guid.NewGuid (), "BannedUser", "Pass", banned)
  let bao = ForumUser.Create (Guid.NewGuid (), "AnotherBao", "Pass", banned)

  let users = Write.writeEntities [ adm; ado; sen; anS; moD; anM; noA; noB; noC; ban; bao ]
  Write.writeEntities users |> ignore
  let moders =
    [ moD; anM ]
    |> List.map ShownUser.CreateFromForumUser
    |> Seq.ofList
  let bannrs =
    [ bao; ban ]
    |> List.map ShownUser.CreateFromForumUser
    |> Seq.ofList

  let something = Guid.NewGuid()

  let userFromDbById           = getUserByGuidString <| (string <| adm.Id)
  let userFromDbByIdFail       = getUserByGuidString (string something)
  let userFromDbByUsername     = getUserByName "AdminUserLoadUsers"
  let userFromDbByUsernameFail = getUserByName "NotUser"

  Write.deleteEntities users

  Assert.Equal (ShownUser.CreateFromForumUser adm |> ok, userFromDbById)
  Assert.Equal (NonExistentUser something |> fail, userFromDbByIdFail)
  Assert.Equal (ShownUser.CreateFromForumUser adm |> ok, userFromDbByUsername)
  Assert.Equal (NonExistentUsername "NotUser" |> fail, userFromDbByUsernameFail)

[<Fact>]
let CanLoadPostsAndThreadsByUsers () =
  let cat   = ForumCategory.Create (Guid.NewGuid (), "cat", false, "desc", [], normaluser.Value) |> Write.writeEntity
  let userA =
    ForumUser.Create (Guid.NewGuid (),
                      "usera",
                      "p",
                      normaluser)
    |> Write.writeEntity
  let userB =
    ForumUser.Create (Guid.NewGuid (),
                      "userb",
                      "p",
                      normaluser)
    |> Write.writeEntity
  let postA =
    ForumPost.Create (Guid.NewGuid (),
                      userA.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 1.,
                      None,
                      "content postA")
    |> Write.writeEntity
  let postB =
    ForumPost.Create (Guid.NewGuid (),
                      userA.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 3.,
                      None,
                      "content postB")
    |> Write.writeEntity
  let postC =
    ForumPost.Create (Guid.NewGuid (),
                      userB.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 5.,
                      None,
                      "content postC")
    |> Write.writeEntity
  let postD =
    ForumPost.Create (Guid.NewGuid (),
                      userA.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 7.,
                      None,
                      "content postD")
    |> Write.writeEntity
  let postE =
    ForumPost.Create (Guid.NewGuid (),
                      userA.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 9.,
                      None,
                      "content postE")
    |> Write.writeEntity
  let threA =
    ForumThread.Create (Guid.NewGuid (),
                        cat.Id,
                        "titA",
                        userA.Id,
                        stdOpenT (postA.Created) userA.Id,
                        postE.Created,
                        postE.Author,
                        [ postA.Id; postB.Id; postE.Id ])
  let threB =
    ForumThread.Create (Guid.NewGuid (),
                        cat.Id,
                        "titB",
                        userB.Id,
                        stdOpenT (postC.Created) userB.Id,
                        postD.Created,
                        postD.Author,
                        [ postC.Id; postD.Id ])

  let expectedPostsUserA =
    [ postA; postB; postD; postE ]
    |> List.sortByDescending (fun p -> p.Created)
    |> List.map ListedPost.CreateFromPost
  let expectedPostsUserB =
    [ postC ]
    |> List.sortByDescending (fun p -> p.Created)
    |> List.map ListedPost.CreateFromPost
  let postsByUserA = getUsersPosts (ShownUser.CreateFromForumUser userA)
  let postsByUserB = getUsersPosts (ShownUser.CreateFromForumUser userB)

  let expectedThreadsUserA =
    [ threA ]
    |> List.map ListedThreadCompact.CreateFromForumThread
  let expectedThreadsUserB =
    [ threB ]
    |> List.map ListedThreadCompact.CreateFromForumThread
  let threadsUserA = getUsersThreads (ShownUser.CreateFromForumUser userA)
  let threadsUserB = getUsersThreads (ShownUser.CreateFromForumUser userB)

  Write.deleteEntities [ postA; postB; postC; postD; postE ]
  Write.deleteEntities [ userA; userB ]
  Write.deleteEntities [ threA; threB ]
  Write.deleteEntity cat

  Seq.zip expectedPostsUserA postsByUserA
  |> Seq.iter Assert.Equal
  Seq.zip expectedPostsUserB postsByUserB
  |> Seq.iter Assert.Equal

  Seq.zip expectedThreadsUserA threadsUserA
  |> Seq.iter Assert.Equal
  Seq.zip expectedThreadsUserB threadsUserB
  |> Seq.iter Assert.Equal

[<Fact>]
let GetPostsJustFine () =
  let postAId = Guid.NewGuid ()
  let userA =
    ForumUser.Create (Guid.NewGuid (),
                      "usera",
                      "p",
                      normaluser)
    |> Write.writeEntity
  let postA =
    ForumPost.Create (postAId,
                      userA.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 1.,
                      None,
                      "content postA")
    |> Write.writeEntity
  let spostA =
    ShownPost.Create (postA.Id,
                      ShownUser.CreateFromForumUser userA,
                      postA.Created,
                      postA.Title,
                      postA.Content)

  let randId      = Guid.NewGuid ()
  let getPostA    = getShownPost postAId
  let failGetPost = getShownPost randId

  Write.deleteEntity userA
  Write.deleteEntity postA

  Assert.Equal (spostA |> ok, getPostA)
  Assert.Equal (NonExistentPost randId |> fail, failGetPost)

[<Fact>]
let BusiThreadFunctionsBehaveAsExpected () =
  let catA  =
    ForumCategory.Create (Guid.NewGuid (),
                          "cat",
                          false,
                          "desc",
                          [],
                          normaluser.Value)
    |> Write.writeEntity
  let catB  =
    ForumCategory.Create (Guid.NewGuid (),
                          "cat",
                          false,
                          "desc",
                          [],
                          normaluser.Value)
    |> Write.writeEntity
  let userA =
    ForumUser.Create (Guid.NewGuid (),
                      "usera",
                      "p",
                      normaluser)
    |> Write.writeEntity
  let userB =
    ForumUser.Create (Guid.NewGuid (),
                      "userb",
                      "p",
                      normaluser)
    |> Write.writeEntity
  let postA =
    ForumPost.Create (Guid.NewGuid (),
                      userA.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 1.,
                      None,
                      "content postA")
    |> Write.writeEntity
  let postB =
    ForumPost.Create (Guid.NewGuid (),
                      userA.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 3.,
                      None,
                      "content postB")
    |> Write.writeEntity
  let postC =
    ForumPost.Create (Guid.NewGuid (),
                      userB.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 5.,
                      None,
                      "content postC")
    |> Write.writeEntity
  let postD =
    ForumPost.Create (Guid.NewGuid (),
                      userA.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 7.,
                      None,
                      "content postD")
    |> Write.writeEntity
  let postE =
    ForumPost.Create (Guid.NewGuid (),
                      userA.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 8.,
                      None,
                      "content postE")
    |> Write.writeEntity
  let postF =
    ForumPost.Create (Guid.NewGuid (),
                      userA.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 9.,
                      None,
                      "content postF")
    |> Write.writeEntity
  let threA =
    ForumThread.Create (Guid.NewGuid (),
                        catA.Id,
                        "titA",
                        userA.Id,
                        stdOpenT postA.Created userA.Id,
                        postE.Created,
                        postE.Author,
                        [ postA.Id; postB.Id; postE.Id ])
    |> Write.writeEntity
  let threB =
    ForumThread.Create (Guid.NewGuid (),
                        catB.Id,
                        "titB",
                        userB.Id,
                        stdOpenT postC.Created userB.Id,
                        postD.Created,
                        postD.Author,
                        [ postC.Id; postD.Id ])
    |> Write.writeEntity
  let threC =
    ForumThread.Create (Guid.NewGuid (),
                        catB.Id,
                        "titC",
                        userB.Id,
                        stdOpenT postC.Created userB.Id,
                        postF.Created,
                        postF.Author,
                        [ postC.Id; postD.Id; postF.Id ])
    |> Write.writeEntity

  let shownUserA   =
    ShownUser.CreateFromForumUser userA
  let shownUserB   =
    ShownUser.CreateFromForumUser userB
  let shownPostA   =
    ShownPost.Create (postA.Id,
                      shownUserA,
                      postA.Created,
                      postA.Title,
                      postA.Content)
  let shownPostB   =
    ShownPost.Create (postB.Id,
                      shownUserA,
                      postB.Created,
                      postB.Title,
                      postB.Content)
  let shownPostC   =
    ShownPost.Create (postC.Id,
                      shownUserB,
                      postC.Created,
                      postC.Title,
                      postC.Content)
  let shownPostD   =
    ShownPost.Create (postD.Id,
                      shownUserA,
                      postD.Created,
                      postD.Title,
                      postD.Content)
  let shownPostE   =
    ShownPost.Create (postE.Id,
                      shownUserA,
                      postE.Created,
                      postE.Title,
                      postE.Content)
  let shownPostF   =
    ShownPost.Create (postF.Id,
                      shownUserA,
                      postF.Created,
                      postF.Title,
                      postF.Content)
  let shownThreadA =
    ShownThread.Create (threA.Id,
                        "titA",
                        shownUserA,
                        stdOpenT postA.Created postA.Author,
                        3,
                        [ shownPostA; shownPostB; shownPostE ])
  let shownThreadB =
    ShownThread.Create (threB.Id,
                        "titB",
                        shownUserB,
                        threB.Activity,
                        2,
                        [ shownPostC; shownPostD ])
  let shownThreadC =
    ShownThread.Create (threC.Id,
                        "titC",
                        shownUserB,
                        threC.Activity,
                        3,
                        [ shownPostC; shownPostD; shownPostF ])


  let threadsInCategoryA = threadsInCats catA.Id
  let threadsInCategoryB = threadsInCats catB.Id
  let failToCategory     = threadsInCats postD.Id

  let shownThreadFromDbA = getShownThread threA.Id
  let shownThreadFromDbB = getShownThread threB.Id
  let shownThreadFromDbC = getShownThread threC.Id
  let failToRetrieve   = getShownThread catA.Id

  Seq.zip [ threA ] threadsInCategoryA
  |> Seq.iter Assert.Equal
  Seq.zip [ threC; threB ] threadsInCategoryB
  |> Seq.iter Assert.Equal
  // Assert.Equal (emptyCategory, failToCategory)

  Assert.Equal (shownThreadA |> ok, shownThreadFromDbA)
  Assert.Equal (shownThreadB |> ok, shownThreadFromDbB)
  Assert.Equal (shownThreadC |> ok, shownThreadFromDbC)
  Assert.Equal (NonExistentThread catA.Id |> fail, failToRetrieve)

  Write.deleteEntities [ catA; catB ]
  Write.deleteEntities [ threA; threB; threC ]
  Write.deleteEntities [ postA; postB; postC; postD; postE; postF ]
  Write.deleteEntities [ userA; userB ]

[<Fact>]
let getMods () =
  let sen =
    ForumUser.Create (Guid.NewGuid (),
                              "SeniorUser1",
                              "Password",
                              senmod)
    |> Write.writeEntity
  let anS =
    ForumUser.Create (Guid.NewGuid (),
                      "SeniorUser2",
                      "Password",
                      senmod)
    |> Write.writeEntity
  let moD =
    ForumUser.Create (Guid.NewGuid (),
                      "ModUser1",
                      "Password",
                      moderator)
    |> Write.writeEntity
  let anM =
    ForumUser.Create (Guid.NewGuid (),
                      "ModUser2",
                      "Password",
                      moderator)
    |> Write.writeEntity
  let noA =
    ForumUser.Create (Guid.NewGuid (),
                      "NormalUserA",
                      "Pass",
                      normaluser)
    |> Write.writeEntity
  let noB =
    ForumUser.Create (Guid.NewGuid (), "NormalUserB", "Pass", normaluser)
    |> Write.writeEntity

  let mods = getMods ()
  let sens = getSenMods ()
  let expS = [ sen; anS ]
  let expM = [ moD; anM ]

  Write.deleteEntities [ sen; anS; moD; anM; noA; noB ]
  List.zip expM mods
  |> List.iter Assert.Equal
  List.zip expS sens
  |> List.iter Assert.Equal
