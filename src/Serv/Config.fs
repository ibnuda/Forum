namespace Serv

open System
open System.IO

open Chiron
open Operators

open Forum.Utilities

module Config =

  module D = Json.Decode

  type UserPaths =
    { SignUp  : string
      Root    : string
      Posts   : string
      Threads : string }
  type CategoryPaths =
    { Root     : string
      Category : string }
  type AdminPaths =
    { Main       : string
      SeniorMods : string
      Moderators : string }

  type PathConfig =
    { Paging  : int
      Token   : string
      Root    : string
      Lost    : string
      Users   : UserPaths
      Posts   : string
      Threads : string
      Cats    : CategoryPaths
      Admin   : AdminPaths }

  type TimeoutConfig =
    { Request         : int
      ExpirationToken : int64 }
    static member Create (req, exp) =
      { Request         = req
        ExpirationToken = exp }

  type ServConfig =
    { Path    : PathConfig
      Timeout : TimeoutConfig }
    static member Create (api, timeout) =
      { Path    = api
        Timeout = timeout }

  module UserPaths =
    let private create s r p t =
      { SignUp  = s
        Root    = r
        Posts   = p
        Threads = t }
    let decode =
      create
      <!> D.required D.string "signup"
      <*> D.required D.string "root"
      <*> D.required D.string "posts"
      <*> D.required D.string "threads"

  module CategoryPaths =
    let private create r c : CategoryPaths =
      { Root     = r
        Category = c }
    let decode =
      create
      <!> D.required D.string "root"
      <*> D.required D.string "category"

  module AdminPaths =
    let private create m s mo =
      { Main        = m
        SeniorMods  = s
        Moderators  = mo }
    let decode =
      create
      <!> D.required D.string "main"
      <*> D.required D.string "seniormods"
      <*> D.required D.string "moderators"

  module PathConfig =
    let private dUserPath  = D.jsonObjectWith UserPaths.decode
    let private dCatePath  = D.jsonObjectWith CategoryPaths.decode
    let private dAdminPath = D.jsonObjectWith AdminPaths.decode
    let private create pag au r s u p t c a =
      { Paging  = pag
        Token   = au
        Root    = r
        Lost    = s
        Users   = u
        Posts   = p
        Threads = t
        Cats    = c
        Admin   = a }
    let decode =
      create
      <!> D.required D.int      "paging"
      <*> D.required D.string   "auth"
      <*> D.required D.string   "root"
      <*> D.required D.string   "lost"
      <*> D.required dUserPath  "users"
      <*> D.required D.string   "posts"
      <*> D.required D.string   "threads"
      <*> D.required dCatePath  "categories"
      <*> D.required dAdminPath "admin"

  module TimeoutConfig =
    let private create req tok = TimeoutConfig.Create (req, tok)
    let decode =
      create
      <!> D.required D.int   "request"
      <*> D.required D.int64 "token"

  module ServConfig =
    let private create path tout = ServConfig.Create (path, tout)
    let decode =
      create
      <!> D.required (D.jsonObjectWith PathConfig.decode)    "path"
      <*> D.required (D.jsonObjectWith TimeoutConfig.decode) "timeout"

  let readConfig () =
    let configfilename = "forum.serv.config.json"
    let lines = File.ReadAllLines configfilename |> String.Concat
    jsonToModel ServConfig.decode lines
