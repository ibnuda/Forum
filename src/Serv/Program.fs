﻿open System
open System.IO

open Chessie.ErrorHandling

open Forum.Utilities
open Forum.ManageUser

open Serv.Config
open Serv.KInterop
open Serv.RoutesRes

[<EntryPoint>]
let main argv =

  match readConfig () with
  | Ok (config, _) ->
    if checkExistenceAdmin  () then
      ()
    else
      printf "please insert password: "
      let pass = askPassword ()
      createAdministrator pass
      |> printfn "%A"

    printfn "%A" config

    let configuredApp =
      ApplicationBuilder.useRouteListFromFreya (rootWithConfig config)

    WebHost.create ()
    |> WebHost.bindTo [| "http://*:5000" |]
    |> WebHost.contentRoot (Directory.GetCurrentDirectory ())
    |> WebHost.configure configuredApp
    |> WebHost.buildAndRun

  | Bad _  ->
    printfn "Config file is not valid."

  0
