module Clie.Utility.Style

open System

open Fable.Import
open Fable.Helpers.React
open Fable.Helpers.React.Props

open Clie.Global

let viewLink page desc =
  a [ ClassName "navbar-item"
      Href (toHash page) ] [
    unbox desc
  ]

let buttonLink onClick elements =
  a [ ClassName "navbar-item"
      OnClick (fun _ -> onClick ())
      OnTouchStart (fun _ -> onClick ())
      Style [unbox ("cursor", "pointer") ] ] elements

let centerStyle direction =
  Style [
    Display "flex"
    FlexDirection direction
    AlignItems "center"
    unbox ("justifyContent", "center")
    Padding "20px 0"
  ]

let words size message =
  span [ Style [ unbox ("fontSize", sprintf "%dpx" size) ] ] [
    unbox message
  ]

let textunb x = text [ ] [ unbox x ]

let [<LiteralAttribute>] private ENTER = 13.

let onEnter message disp =
  function
  | (event : React.KeyboardEvent) when event.keyCode = ENTER ->
    event.preventDefault ()
    disp message
  | _ -> ()
  |> OnKeyDown
