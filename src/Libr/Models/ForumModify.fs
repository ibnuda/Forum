namespace Forum

open Unif.ForumModels

module ForumModify =

  open Utilities

  let jsonToQualif   = jsonToModel Qualification.decode

  let qualifToJson   = modelToJson Qualification.encode

  let createUser id username password qualif =
    ForumUser.Create (id, username, password, qualif)

  let createPost id author created title content =
    ForumPost.Create (id, author, created, title, content)

  let editpassword password user =
    setUserPassword password user

  let editPost title content forumPost =
    forumPost
    |> setPostTitle title
    |> setPostContent content

  let createCategory id name locked desc mods minReq =
    ForumCategory.Create (id, name, locked, desc, mods, minReq)

  let lockCategory category =
    setCategoryLocked true category

  let unlockCategory category =
    setCategoryLocked false category

  let createThreadFromPost id catId title (forumPost : ForumPost) =
    let stdOpenT at by = Activity.Create (1, false, at, by)
    let title =
      match title with
      | Some thing  -> thing
      | None        ->
        forumPost.Content |> Seq.truncate 50 |> System.String.Concat
    let activity = stdOpenT forumPost.Created forumPost.Author
    ForumThread.Create (id, catId,
                        title, forumPost.Author,
                        activity, forumPost.Created,
                        forumPost.Author, [ forumPost.Id ])

  let replyThread (forumPost : ForumPost) forumThread =
    forumThread
    |> getThreadReplies
    |> flip List.append [ forumPost.Id ]
    |> flip setThreadReplies forumThread
    |> setThreadLastActive forumPost.Created
    |> setThreadLastBy forumPost.Author

  let isAModInCategory userId category =
    List.contains userId category.Moderatrolls

  let addModInCategory (user : ForumUser) (category : ForumCategory) =
    category
    |> getCategoryModerators
    |> flip List.append [ user.Id ]
    |> flip setCategoryModerators category

  let removeModInCategory (moderator : ForumUser) (category : ForumCategory) =
    category
    |> getCategoryModerators
    |> List.filter (fun x -> x <> moderator.Id)
    |> flip setCategoryModerators category
