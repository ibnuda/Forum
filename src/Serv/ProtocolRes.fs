namespace Serv

open System.IO

open Chessie.ErrorHandling

open Unif.Errors
open Unif.ResponseModels
open Unif.Auth

module ProtocolRes =

  type Soption = string option
  type ARCRes  = AsyncReplyChannel<Result<ForumResponse, Failures>>

  type AnotherInOut =
    | AuthCreate         of ARCRes * Pl   : Stream
    | UserCreate         of ARCRes * Pl   : Stream
    | UserDataGet        of ARCRes * UId  : Soption
    | UserPostsGet       of ARCRes * UId  : Soption
    | UserThreadsGet     of ARCRes * UId  : Soption
    | PostGet            of ARCRes * PId  : Soption
    | PostEdit           of ARCRes * Tokn : Token   * PId : Soption * Pl : Stream
    | ThreadCreate       of ARCRes * Tokn : Token   * CId : Soption * Pl : Stream
    | ThreadGet          of ARCRes * TId  : Soption * Pg  : Soption
    | ThreadReply        of ARCRes * Tokn : Token   * TId : Soption * Pl : Stream
    | ImageGet           of ARCRes * Path : Soption
    | ImageCreate        of ARCRes * Tokn : Token   * Pl  : Stream
    | ChangeUserQualif   of ARCRes * Tokn : Token * TId : Soption  * Pl : Stream
    | GembokThreadCok    of ARCRes * Tokn : Token * TId : Soption * Pl : Stream
    | GembokCatCok       of ARCRes * Tokn : Token * CId : Soption * Pl : Stream
    | CatMain            of ARCRes
    | CatGet             of ARCRes * CId  : Soption * Page : Soption
    | CatCreate          of ARCRes * Tokn : Token * Pl : Stream
