namespace Serv

open Chessie.ErrorHandling

open Unif.MainModels

open Forum.Utilities
open Forum.ServeUser
open Forum.ServePost
open Forum.ServeThread
open Forum.ServeCategory
open Forum.ServeImage

open Unif.ResponseModels

module ApisServe =

  [<RequireQualifiedAccessAttribute>]
  module Serve =

    [<RequireQualifiedAccessAttribute>]
    module Exists =
      let user id =
        match id with
        | None   -> false
        | Some _ -> true

    [<RequireQualifiedAccessAttribute>]
    module User =
      let private getId uId =
        uId
        |>  failIfNoneOrEmpty
        >>= strToGuid
        >>= getShownUser

      let get uId =
        uId
        |> getId
        |> lift DataUser

      let getName name =
        name
        |>  failIfNoneOrEmpty
        >>= getUserByName
        |>  lift DataUser

      let getPosts =
        getId >> lift getUsersPosts >> lift DataPosts

      let getThreads =
        getId >> lift getUsersThreads >> lift DataListedThreads

    [<RequireQualifiedAccessAttribute>]
    module Post =
      let get pId =
        pId
        |>  failIfNoneOrEmpty
        >>= strToGuid
        >>= getShownPost
        |>  lift DataPost

    [<RequireQualifiedAccessAttribute>]
    module Thread =
      let get tId =
        tId
        |>  failIfNoneOrEmpty
        >>= strToGuid
        >>= getApiShownThread
        |>  lift DataSingleThread

      let getChunk perpage tId page =
        page
        |>  strToInt
        >>= fun p ->
          tId
          |>  failIfNoneOrEmpty
          >>= strToGuid
          >>= getChunkedApiShownThread p perpage
          |>  lift DataSingleThread

    [<RequireQualifiedAccessAttribute>]
    module Category =
      let getMain () =
        getMainCategories ()
        |> DataMainCategories
        |> ok

      let get cId =
        cId
        |>  failIfNoneOrEmpty
        >>= strToGuid
        >>= getShownCategory
        |>  lift DataSingleCategory

      let getChunk perpage cId page =
        page
        |>  strToInt
        >>= fun p ->
          cId
          |>  failIfNoneOrEmpty
          >>= strToGuid
          >>= getChunkedShownCategory p perpage
          |>  lift DataSingleCategory

    [<RequireQualifiedAccessAttribute>]
    module Image =
      let get iId =
        iId
        |> failIfNoneOrEmpty
        >>= strToGuid
        >>= getImage

    [<RequireQualifiedAccessAttribute>]
    module Mods =
      let mods () =
        getMods ()
        |> List.map (ShownUser.CreateFromForumUser >> DataUser)

      let senmods () =
        getSenMods ()
        |> List.map (ShownUser.CreateFromForumUser >> DataUser)
