namespace Unif

open Chessie.ErrorHandling
open Chiron

open Errors

module Utilities =

  module D = Json.Decode
  module E = Json.Encode

  let jsonToModel (decoder : JsonObject -> JsonResult<'T>) jsonString =
    let parsingResult =
      jsonString
      |> Json.parse
      |> JsonResult.bind (D.jsonObjectWith decoder)
    match parsingResult with
    | JPass result -> result |> ok
    | JFail _ -> NotAValidPayload jsonString |> fail

  let inline modelToJson (encoder : 'T -> JsonObject -> JsonObject) model =
    model
    |> E.jsonObjectWith encoder
    |> Json.format
