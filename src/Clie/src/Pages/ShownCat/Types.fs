module Clie.Pages.ShownCat.Types

open System

open Clie.Global

type ListedThread =
  { id         : string
    title      : string
    op         : UserInformation
    replies    : int
    lastposter : string
    lastact    : string }

type ShownCategory =
  { id      : string
    name    : string
    desc    : string
    threads : ListedThread list }
  static member Empty =
    { id = ""
      name = ""
      desc = ""
      threads = [ ] }

type ShownCatModel =
  { Cats : ShownCategory
    ErrM : string }
  static member Empty =
    { Cats = ShownCategory.Empty
      ErrM = "" }

let setcateg shown model =
  { model with Cats = shown }

let seterror error model =
  { model with ErrM = error }
