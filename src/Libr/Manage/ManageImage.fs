namespace Forum

open Chessie.ErrorHandling

open Unif.Errors
open Unif.ForumModels

module ManageImage =

  open ImagesReadWrite
  open DocReadWrite

  let saveImages execId stream =
    match Read.load<ForumUser> execId with
    | None      -> TFAreYou |> fail
    | Some user ->
      saveFiles user.Id stream
      |> lift (Seq.toList >> Write.writeEntities)
