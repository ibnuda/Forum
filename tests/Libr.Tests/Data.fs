module Data

open System

open Unif.ForumModels
open Unif.MainModels
open Forum.ForumModify

let guidA = Guid.Parse "0123456789abcdeffedcba9876543200"
let guidB = Guid.Parse "0123456789abcdeffedcba9876543201"
let guidC = Guid.Parse "0123456789abcdeffedcba9876543202"
let guidD = Guid.Parse "0123456789abcdeffedcba9876543203"
let guidE = Guid.Parse "0123456789abcdeffedcba9876543204"

let postIds =
  [ Guid.Parse "0123456789abcdeffedcba9876543210"
    Guid.Parse "0123456789abcdeffedcba9876543211"
    Guid.Parse "0123456789abcdeffedcba9876543212"
    Guid.Parse "0123456789abcdeffedcba9876543213"
    Guid.Parse "0123456789abcdeffedcba9876543214"
    Guid.Parse "0123456789abcdeffedcba9876543215" ]

let posts =
  postIds
  |> List.map (fun x -> ForumPost.Create(x, guidA, DateTime.FromBinary(10L), None, "This is a thread's title."))

let standardAdministraTrollQualification = Qualification.Create (4, 4, "Administrator")
let standardSeniorModeraTrollQualification = Qualification.Create (3, 3, "Senior Moderator")
let standardModeraTrollQualification = Qualification.Create (2, 2, "Moderator")
let standardNormalUser = Qualification.Create (1, 1, "User")
let standardBanned = Qualification.Create (-1, -1, "Banned")

let standardLocked at by = Activity.Create (0, true, at, by)
let standardReopened at by = Activity.Create (1, false, at, by)

let aNormalUser = ForumUser.Create (guidA, "NormalUser", "PasswordUser", standardNormalUser)
let anotherNormalUser = ForumUser.Create (guidE, "AnotherNormalUser", "PasswordUser", standardNormalUser)
let aBannedUser = ForumUser.Create (guidB, "BannedUser", "PasswordBanned", standardBanned)
let aJailedUser = { aBannedUser with Qualification = standardBanned }
let anUnbannedUser = { aBannedUser with Qualification = standardNormalUser }
let aModerator = ForumUser.Create (guidC, "Moderator", "PasswordModerator", standardModeraTrollQualification)
let anotherModerator = { aModerator with Id = guidD }
let aSeniorMod = { anotherModerator with Qualification = standardSeniorModeraTrollQualification }
let anAdministrator = ForumUser.Create (guidD, "Administrator", "PasswordAdministrator", standardAdministraTrollQualification)
// let uid = Guid.Parse "a123456789abcdeffedcba9876543210"
let createdUser = ForumUser.Create (Guid.NewGuid (), "MarcoPolo", "UwU", Qualification.Create(6, 6, "Yes"))
let edittedUser =
  createdUser
  |> setUserPassword "LOL"
  |> setUserQualificationName "LOL"

let aNormalUserJson =
    """{"id":"0123456789abcdeffedcba9876543200","""
  + """"username":"NormalUser","""
  + """"qualification":{"""
  + """"id":1,"""
  + """"value":1,"""
  + """"name":"User"}"""
  + """}"""

let aNormalUserJsonWithPassword =
    """{"id":"0123456789abcdeffedcba9876543200","""
  + """"username":"NormalUser","""
  + """"password":"PasswordUser","""
  + """"qualification":{"""
  + """"id":1,"""
  + """"value":1,"""
  + """"name":"User"}"""
  + """}"""

let aNormalPost =
  ForumPost.Create (
    guidA, guidB, DateTime.FromBinary(10L),
    Some "This is a title",
    "This is a normal post. Can be short. Can be looong.")
let aNormalPostWithoutTitle = { aNormalPost with Title = None }

let aNormalPostJson =
    """{"id":"0123456789abcdeffedcba9876543200","""
  + """"author":"0123456789abcdeffedcba9876543201","""
  + """"created":10,"""
  + """"title":"This is a title","""
  + """"content":"This is a normal post. Can be short. Can be looong."}"""

let aNormalPostWithoutTitleJson =
    """{"id":"0123456789abcdeffedcba9876543200","""
  + """"author":"0123456789abcdeffedcba9876543201","""
  + """"created":10,"""
  + """"title":null,"""
  + """"content":"This is a normal post. Can be short. Can be looong."}"""

let aNormalCategory =
  ForumCategory.Create
    (Guid.Empty, "Normal Category", false, "A Description", [ aModerator.Id ], QualifValue.ToInt Normal)

let aCat =
  ForumCategory.Create
    (Guid.Empty, "CatName", false, "CatDesc", [], 1)

let aCatPlusMod = addModInCategory aModerator aCat

let anOpenThread =
  ForumThread.Create (guidD,
                      aNormalCategory.Id,
                      "This is a thread's title.",
                      posts.Head.Author,
                      (standardReopened (DateTime.FromBinary(11L)) anAdministrator.Id),
                      posts.Head.Created,
                      posts.Head.Author,
                      [ postIds.Head ] )
let anOpenThreadX =
  ForumThread.Create (guidD,
                      aNormalCategory.Id,
                      "This is a thread's title.",
                      posts.Head.Author,
                      (standardReopened (DateTime.FromBinary(10L)) posts.Head.Author),
                      posts.Head.Created,
                      posts.Head.Author,
                      [ postIds.Head ] )

let anotherOpenThread : ForumThread = { anOpenThread with Title = "Title." }

let aRepliedThread = { anOpenThread with Replies = postIds }

let aLockedThread = { aRepliedThread with Activity = standardLocked (DateTime.FromBinary(11L)) anAdministrator.Id }

let realUserA  = ForumUser.Create (Guid.NewGuid(), "MarcoPollo", "where is Marco?", standardNormalUser)
let shownUserA = ShownUser.CreateFromForumUser realUserA
