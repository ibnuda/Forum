namespace Forum

open Chessie.ErrorHandling
open Marten

open Unif.Errors
open Unif.ForumModels

module ManageThread =

  open Utilities
  open BusinessRules
  open DocHelpers
  open DocReadWrite

  let initThread authorId threadId categoryId title postId =
    match Read.load<ForumUser> authorId,
          Read.load<ForumCategory> categoryId,
          Read.load<ForumPost> postId with
    | None, _, _                            -> NonExistentUser authorId |> fail
    | Some _, None, _                       -> NonExistentCategory categoryId |> fail
    | Some _, Some  _, None                 -> NonExistentPost postId |> fail
    | Some author, Some category, Some post ->
      initForumThread author threadId category title post
      |> lift Write.writeEntity

  let getCatFromThreadId threadId =
    match Read.load<ForumThread> threadId with
    | None -> None
    | Some thread ->
      let cat =
        use session = store.LightweightSession ()
        session
        |> Session.query<ForumCategory>
        |> Queryable.filter <@ fun c -> c.Id = thread.CategoryId @>
        |> Queryable.tryExactlyOne
      cat

  let replyThread posterId postId threadId =
    match Read.load<ForumUser> posterId,
          Read.load<ForumPost> postId,
          Read.load<ForumThread> threadId with
    | None, _, _                          -> NonExistentUser posterId |> fail
    | Some _, None, _                     -> NonExistentPost postId |> fail
    | Some _, Some _, None                -> NonExistentThread threadId |> fail
    | Some poster, Some post, Some thread ->
      match getCatFromThreadId thread.Id with
      | Some category ->
        if category.MinimalRequirementToParticipate <= poster.Qualification.Value then
          replyForumThread poster post thread
          |> lift Write.writeEntity
        else
          NotQualifiedToPostInThisForumThread (category.Id, poster.Qualification.Name) |> fail
      | None ->
        NonExistentCategory thread.CategoryId |> fail

  let lockThread lockerId threadId =
    match Read.load<ForumUser> lockerId,
          Read.load<ForumThread> threadId with
    | None, _                -> NonExistentUser lockerId |> fail
    | Some _, None           -> NonExistentThread threadId |> fail
    | Some user, Some thread ->
      lockForumThread (nao ()) user thread
      |> lift Write.writeEntity

  let unlockThread unlockerId threadId =
    match Read.load<ForumUser> unlockerId,
          Read.load<ForumThread> threadId with
    | None, _                -> NonExistentUser unlockerId |> fail
    | Some _, None           -> NonExistentThread threadId |> fail
    | Some user, Some thread ->
      unlockForumThread (nao ()) user thread
      |> lift Write.writeEntity
