namespace Forum

open Chessie.ErrorHandling
open Marten

open Unif.Errors
open Unif.ForumModels
open Unif.Auth

module LockPreparations =

  open Utilities
  open DocHelpers

  let userRequestFromJson something =
    something
    |> jsonToModel UserRequest.decode

  let checkUser (req : UserRequest) : Result<ForumUser, Failures> =
    use session = store.LightweightSession ()
    session
    |> Session.query<ForumUser>
    |> Queryable.filter <@ fun u -> u.Username = req.Username &&
                                    u.Password = req.Password @>
    |> Queryable.tryExactlyOne
    |> failIfNone (NonExistentUsername req.Username)
