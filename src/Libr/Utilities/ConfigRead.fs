namespace Forum

open System
open System.IO

module ConfigRead =

  open Utilities

  open ConfigDec

  // TODO: Fix this.
  let readConfig () =
    let configfilename = "forum.libr.config.json"
    let lines = File.ReadAllLines configfilename |> String.Concat
    jsonToModel ConfigurationDec.decode lines
