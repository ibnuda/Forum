module Clie.Pages.Category.Views

open System

open Elmish

open Fable.Helpers.React
open Fable.Helpers.React.Props

open Clie.Global
open Clie.Messages

open Types

let viewcattr (cat : Category) =
  tr [ ] [
    td [ ] [ a [ Href (toHash <| Page.CategoryId cat.id) ] [ str cat.name ] ]
    td [ ] [ text [ ] [ str cat.desc ] ]
    td [ ] [ text [ ] [ str <| cat.threads.ToString () ] ]
    td [ ] [ text [ ] [ str <| cat.posts.ToString () ] ]
    td [ ] [ text [ ] [ str cat.lastthread ] ]
    td [ ] [ text [ ] [ str cat.lastuser ] ]
    td [ ] [ text [ ] [ str <| cat.lastactive.ToString () ] ]
  ]

let view (model : CategoryModel) dispatch =
  match String.IsNullOrEmpty model.ErrorMessa with
  | false ->
    div [ ClassName "error" ] [
      str model.ErrorMessa
    ]
  | true ->
    div [ ClassName "container" ] [
      nav [ ClassName "breadcrumb" ] [
        li [ ] [
          a [ Href (toHash Page.Main) ] [ str "Home" ]
        ]
        li [ ] [
          a [ Href (toHash Page.Category) ] [ str "Categories" ]
        ]
      ]
      table [ ClassName "table" ] [
        thead [ ] [
          tr [ ] [
            th [ ] [ text [ ] [ str "Name" ] ]
            th [ ] [ text [ ] [ str "Desc." ] ]
            th [ ] [ text [ ] [ str "Threads" ] ]
            th [ ] [ text [ ] [ str "Replies" ] ]
            th [ ] [ text [ ] [ str "Last Thread" ] ]
            th [ ] [ text [ ] [ str "Last User" ] ]
            th [ ] [ text [ ] [ str "Last Reply" ] ]
          ]
        ]
        tbody [ ] [
          for cat in model.categories.categories do
            yield viewcattr cat
        ]
      ]
    ]
