module Clie.Pages.Login.State

open System

open Fable.Core
open Fable.Core.JsInterop

open Fable.PowerPack
open Fable.PowerPack.Fetch.Fetch_types
open Elmish

open Clie.Global
open Clie.Messages
open Clie.Utility.Utility

open Types

let private authReq (req : UserRequest) =
  promise {
    if String.IsNullOrEmpty req.username then
      return! failwith "Please insert username."
    if String.IsNullOrEmpty req.password then
      return! failwith "Please insert password."
    return! sendReq req "" "http://localhost:5000/token"
  }

let private auth req =
  Cmd.ofPromise authReq req TokenGet AuthError

let init model : LoginModel * Cmd<LoginMessages> =
  match model with
  | None ->
    LoginModel.Empty, Cmd.none
  | Some user ->
    { State = LoginState.LoggedInState user.Token
      LoginReq = UserRequest.Create (user.Username, user.Token)
      ErrMess = "" },
    Cmd.none

let update message model : LoginModel * Cmd<LoginMessages> =
  match message with
  | LoginMessages.TokenGet token ->
    model
    |> setstat (LoggedInState token)
    |> setpass "",
    [ ]
  | LoginMessages.SetUsername username ->
    setname username model,
    [ ]
  | LoginMessages.SetPassword password ->
    setpass password model,
    [ ]
  | LoginMessages.AuthError except ->
    setmess except.Message model,
    [ ]
  | LoginMessages.ClickLogin ->
    model,
    auth model.LoginReq
