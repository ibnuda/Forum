namespace Unif

open System

open Chiron
open Operators

module MainModels =

  open Errors
  open ForumModels

  type ShownUser =
    { Id            : Guid
      Username      : string
      Qualification : Qualification }
    static member Create (id, username, qualif) =
      { Id = id
        Username = username
        Qualification = qualif }
    static member CreateFromForumUser (user : ForumUser) =
      ShownUser.Create (user.Id, user.Username, user.Qualification)
    static member Empty =
      ShownUser.Create (Guid.Empty, String.Empty, Qualification.Create (-6, -6, "Non existent."))

  type ShownPost =
    { Id      : Guid
      Author  : ShownUser
      Created : DateTime
      Title   : string option
      Content : string }
    static member Create (id, author, created, title, content) =
      { Id      = id
        Author  = author
        Created = created
        Title   = title
        Content = content }

  type ListedPost =
    { Id      : Guid
      Created : DateTime
      Content : string }
    static member CreateFromPost (post : ForumPost) =
      { Id      = post.Id
        Created = post.Created
        Content = post.Content }

  type ListedThread =
    { Id         : Guid
      Title      : string
      OP         : ShownUser
      Replies    : int
      LastPoster : string
      LastActive : DateTime }

  type ListedThreadCompact =
    { Id           : Guid
      CategoryId   : Guid
      Title        : string
      LastActive   : DateTime
      RepliesCount : int }
    static member CreateFromForumThread (thread : ForumThread) =
      { Id           = thread.Id
        CategoryId   = thread.CategoryId
        Title        = thread.Title
        LastActive   = thread.LastActive
        RepliesCount = thread.Replies.Length }

  type ShownThread =
    { Id             : Guid
      Title          : string
      OriginalPoster : ShownUser
      Status         : Activity
      RepliesCount   : int
      Replies        : ShownPost list }
    static member Create (id, title, op, status, repcount, rep) =
      { Id             = id
        Title          = title
        OriginalPoster = op
        Status         = status
        RepliesCount   = repcount
        Replies        = rep }

  type ApiShownThread =
    { Id      : Guid
      Title   : string
      Status  : Activity
      Replies : Guid list }
    static member Create (id, title, status, rep) =
      { Id      = id
        Title   = title
        Status  = status
        Replies = rep }
    static member CreateFromForumThread (thr : ForumThread) =
      ApiShownThread.Create (thr.Id, thr.Title, thr.Activity, thr.Replies)

  type StatusThread =
    { Id           : Guid
      Title        : string
      Activity     : Activity
      LastActivity : DateTime
      LastPost     : Guid }
    static member CreateFromForumThread (thread : ForumThread) : StatusThread =
      { Id           = thread.Id
        Title        = thread.Title
        Activity     = thread.Activity
        LastActivity = thread.LastActive
        LastPost     = List.last thread.Replies}

  type ListedCategory =
    { Id         : Guid
      Name       : string
      Desc       : string
      Threads    : int
      Posts      : int
      LastThread : string
      LastUser   : string
      LastActive : DateTime }
    static member Create (id, name, desc, threads, posts, lthr, luser, lact) =
      { Id         = id
        Name       = name
        Desc       = desc
        Threads    = threads
        Posts      = posts
        LastThread = lthr
        LastUser   = luser
        LastActive = lact }

  type ShownCategory =
    { Id       : Guid
      Name     : string
      Desc     : string
      Threads  : ListedThread list }
    static member Create (id, name, desc, threads) =
      { Id       = id
        Name     = name
        Desc     = desc
        Threads  = threads }

  type ApiShownCategory =
    { Id       : Guid
      Name     : string
      Desc     : string
      Threads  : Guid list }
    static member Create (id, name, desc, threads) =
      { Id       = id
        Name     = name
        Desc     = desc
        Threads  = threads }

  type StatusCategory =
    { Id     : Guid
      Name   : string
      Locked : bool
      Mods   : Guid list }
    static member CreateFromForumCategory (category : ForumCategory) =
      { Id     = category.Id
        Name   = category.Name
        Locked = category.Locked
        Mods   = category.Moderatrolls }

  type ShownError =
    { What   : string
      By     : Guid option
      Target : Guid option
      On     : string option
      Reason : string option }

    static member Create (what, by, target, on, reason) =
      { What   = what
        By     = by
        Target = target
        On     = on
        Reason = reason }

    static member CreateFromFailure =
      function
      | Argument ->
            ("Argument Exception.", None, None, None, Some "I don't know.")
      | AlreadyAModInCategory (gid, name) ->
            ("Cannot add mod.", None, Some gid, Some name, Some "Already mod.")
      | AlreadyBanned gid ->
            ("Cannot unban user.", None, Some gid, None, Some "Already banned.")
      | CannotCreateThreadInLockedCategory name ->
            ("Cannot post.", None, None, Some name, Some "Thread locked.")
      | CannotLockLockedThread gid ->
            ("Cannot lock thread.", None, Some gid, None, Some "Already locked.")
      | CannotReplyLockedThread gid ->
            ("Cannot reply thread", None, Some gid, None, Some "Thread locked.")
      | CannotSelfBan ->
            ("Cannot self ban.", None, None, None, None)
      | CannotUnbanNonBannedUser gid ->
            ("Cannot unban user.", None, Some gid, None, Some "Is not banned.")
      | CannotUnlockNotLockedThread gid ->
            ("Cannot unlock thread", None, Some gid, None, Some "Not locked.")
      | CategoryAlreadyLocked gid ->
            ("Cannot lock category.", None, Some gid, None, Some "Already locked.")
      | CategoryAlreadyUnlocked gid ->
            ("Cannot unlock category.", None, Some gid, None, Some "Not locked.")
      | EmptyUpload ->
            ("Cannot upload.", None, None, None, Some "Please upload something.")
      | EmptyQuery ->
            ("Cannot find that.", None, None, None, Some "Please input something.")
      | InvalidCommand command ->
            ("Cannot execute command.", None, None, Some command, None)
      | InvalidId iid ->
            ("Non existent item.", None, None, Some iid, Some "Invalid Guid.")
      | InvalidPage pid ->
            ("Page does not exist.", None, None, Some pid, Some "Invalid integer.")
      | NonExistentCategory gid ->
            ("Non existent category.", None, Some gid, None, None)
      | NonExistentPost gid ->
            ("Non existent post.", None, Some gid, None, None)
      | NonExistentThread gid ->
            ("Non existent thread.", None, Some gid, None, None)
      | NonExistentUser gid ->
            ("Non existent user.", None, Some gid, None, None)
      | NonExistentUsername name ->
            ("Non existent username.", None, None, Some name, None)
      | NotAModInCategory (gid, name) ->
            ("Cannot remove mod.", None, Some gid, Some name, Some "Not a mod in that category.")
      | NotAUser gid ->
            ("Not a user.", None, Some gid, None, None)
      | NotAValidPayload name ->
            ("Not a valid payload.", None, None, Some name, None)
      | NotNeeded ->
            ("Carry on.", None, None, None, None)
      | NotQualifiedToAddMod gid ->
            ("Cannot add mod.", Some gid, None, None, Some "Not qualified.")
      | NotQualifiedToChangePassword gid ->
            ("Not a user.", Some gid, None, None, None)
      | NotQualifiedToChangeQualification (name, nam) ->
            ("Cannot change qualification.", None, None, Some (name + " and" + nam), Some "Not qualified.")
      | NotQualifiedToCreateCategory (gid, name) ->
            ("Cannot create category.", None, Some gid, Some name, Some "Not qualified.")
      | NotQualifiedToCreateThreadInCategory (name, nam) ->
            ("Cannot create thread.", None, None, Some (name + " and " + nam), Some "Not qualified.")
      | NotQualifiedToEditPost (gid, gie) ->
            ("Cannot edit post", Some gid, Some gie, None, Some "Not the author of the post.")
      | NotQualifiedToLockThread (gid, name) ->
            ("Cannot lock thread.", None, Some gid, Some name, Some "Not qualified.")
      | NotQualifiedToLockCategory (gid, name) ->
            ("Cannot lock category.", None, Some gid, Some name, Some "Not qualified.")
      | NotQualifiedToUnlockCategory (gid, name) ->
            ("Cannot unlock category.", None, Some gid, Some name, Some "Not qualified.")
      | NotQualifiedToMoveCategory (gid, gie, gif) ->
            ("Cannot move category", Some gid, Some gie, Some (gif.ToString()), Some "Not qualified.")
      | NotQualifiedToFreeCategory gid ->
            ("Cannot free category", None, Some gid, None, Some "Not qualified.")
      | NotQualifiedToPostInThisForumThread (gid, name) ->
            ("Cannot post here", None, Some gid, Some name, Some "Not qualified.")
      | NotQualifiedToUnlockThread (gid, name) ->
            ("Cannot unlock thread", None, Some gid, Some name, Some "Not qualified.")
      | NotTheAuthor (gid, gie) ->
            ("Cannot edit.", Some gid, Some gie, None, Some "Not the author.")
      | PostAuthorAndPosterNotTheSame (gid, gie) ->
            ("Cannot create thread.", Some gid, Some gie, None, Some "Not the author of the post.")
      | SpecifiedPageNotFound (gid, inte) ->
            ("Cannot load thread's page.", None, Some gid, Some (string inte), Some "Page does not exist.")
      | TFAreYou ->
            ("Who are you?", None, None, None, Some "Please provide some kind of identity.")
      | UsernameAlreadyExists username ->
            ("Cannot create user account", None, None, Some username, Some "The same username already exists.")
      | _ ->
            ("Unspecified error.", None, None, None, None)
      >> ShownError.Create

  module D  = Json.Decode
  module E  = Json.Encode
  module ID = Inference.Json.Decode
  module IE = Inference.Json.Encode

  let private eOptionString = E.optionWith E.string
  let private eListGuid     = E.listWith   E.guid

  module ShownUser =
    let private create id username qualif = ShownUser.Create (id, username, qualif)
    let private eQualif = E.jsonObjectWith Qualification.encode
    let private dQualif = D.jsonObjectWith Qualification.decode

    let encode (user : ShownUser) jsonObject =
      jsonObject
      |> E.required E.guid   "id"             user.Id
      |> E.required E.string "username"       user.Username
      |> E.required eQualif  "qualification"  user.Qualification
    let decode =
      create
      <!> D.required D.guid   "id"
      <*> D.required D.string "username"
      <*> D.required dQualif  "qualification"

  module ListedPost =
    let encode (post : ListedPost) jsonObject =
      jsonObject
      |> E.required E.guid      "id"      post.Id
      |> E.required E.dateTime  "created" post.Created
      |> E.required E.string    "content" post.Content

  module ListedPosts =
    let private eListedPost = E.jsonObjectWith ListedPost.encode
    let private lListedPost = E.listWith eListedPost
    let encode (posts : ListedPost list) jsonObject =
      jsonObject
      |> E.required lListedPost "posts" posts

  module ShownPost =
    let private eShownUser = E.jsonObjectWith ShownUser.encode
    let encode (post : ShownPost) jsonObject =
      jsonObject
      |> E.required E.guid        "id"      post.Id
      |> E.required eShownUser    "author"  post.Author
      |> E.required E.dateTime    "created" post.Created
      |> E.required eOptionString "title"   post.Title
      |> E.required E.string      "content" post.Content

  module ListedThread =
    let private eShownUser = E.jsonObjectWith ShownUser.encode
    let encode (thread : ListedThread) jsonObject =
      jsonObject
      |> E.required E.guid     "id"         thread.Id
      |> E.required E.string   "title"      thread.Title
      |> E.required eShownUser "op"         thread.OP
      |> E.required E.int      "replies"    thread.Replies
      |> E.required E.string   "lastposter" thread.LastPoster
      |> E.required E.dateTime "lastact"    thread.LastActive

  module ListedThreadCompact =
    let encode (thread : ListedThreadCompact) jsonObject =
      jsonObject
      |> E.required E.guid     "id"           thread.Id
      |> E.required E.guid     "catid"        thread.CategoryId
      |> E.required E.string   "title"        thread.Title
      |> E.required E.dateTime "lastactive"   thread.LastActive
      |> E.required E.int      "repliescount" thread.RepliesCount

  module ListedThreadCompacts =
    let private eListedThread = E.jsonObjectWith ListedThreadCompact.encode
    let private lListedThread = E.listWith eListedThread
    let encode (threads : ListedThreadCompact list) jsonObject =
      jsonObject
      |> E.required lListedThread "threads" threads

  module ShownThread =
    let private eShownUser = E.jsonObjectWith ShownUser.encode
    let private eShownPost = E.jsonObjectWith ShownPost.encode
    let private eActivity  = E.jsonObjectWith Activity.encode
    let private eReplies   = E.listWith eShownPost
    let encode (thread : ShownThread) jsonObject =
      jsonObject
      |> E.required E.guid      "id"              thread.Id
      |> E.required E.string    "title"           thread.Title
      |> E.required eShownUser  "originalposter"  thread.OriginalPoster
      |> E.required eActivity   "status"          thread.Status
      |> E.required E.int       "repliestcount"   thread.RepliesCount
      |> E.required eReplies    "replies"         thread.Replies

  module ApiShownThread =
    let private eActivity = E.jsonObjectWith Activity.encode
    let encode (thread : ApiShownThread) jsonObject =
      jsonObject
      |> E.required E.guid    "id"      thread.Id
      |> E.required E.string  "title"   thread.Title
      |> E.required eActivity "status"  thread.Status
      |> E.required eListGuid "replies" thread.Replies

  module StatusThread =
    let private eActivity = E.jsonObjectWith Activity.encode
    let encode (thread : StatusThread) jsonObject =
      jsonObject
      |> E.required E.guid     "id"         thread.Id
      |> E.required E.string   "title"      thread.Title
      |> E.required eActivity  "activity"   thread.Activity
      |> E.required E.dateTime "lastactive" thread.LastActivity
      |> E.required E.guid     "lastpost"   thread.LastPost

  module ListedCategory =
    let encode (category : ListedCategory) jsonObject =
      jsonObject
      |> E.required E.guid     "id"         category.Id
      |> E.required E.string   "name"       category.Name
      |> E.required E.string   "desc"       category.Desc
      |> E.required E.int      "threads"    category.Threads
      |> E.required E.int      "posts"      category.Posts
      |> E.required E.string   "lastthread" category.LastThread
      |> E.required E.string   "lastuser"   category.LastUser
      |> E.required E.dateTime "lastactive" category.LastActive

  module ListedCategories =
    let encode (categories : ListedCategory list) jsonObject =
      jsonObject
      |> E.required (E.listWith (E.jsonObjectWith ListedCategory.encode)) "categories" categories

  module ShownCategory =
    let private eListedThr  = E.jsonObjectWith ListedThread.encode
    let private eListedThrs = E.listWith eListedThr
    let encode (category : ShownCategory) jsonObject =
      jsonObject
      |> E.required E.guid      "id"       category.Id
      |> E.required E.string    "name"     category.Name
      |> E.required E.string    "desc"     category.Desc
      |> E.required eListedThrs "threads"  category.Threads

  module ApiShownCategory =
    let encode (category : ApiShownCategory) jsonObject =
      jsonObject
      |> E.required E.guid    "id"       category.Id
      |> E.required E.string  "name"     category.Name
      |> E.required E.string  "desc"     category.Desc
      |> E.required eListGuid "threads"  category.Threads

  module StatusCategory =
    let encode (category : StatusCategory) jsonObject =
      jsonObject
      |> E.required E.guid "id" category.Id
      |> E.required E.string "name" category.Name
      |> E.required E.bool "locked" category.Locked
      |> E.required eListGuid "mods" category.Mods

  module ShownError =
    let encode (error : ShownError) jsonObject =
      jsonObject
      |> E.required E.string "what"   error.What
      |> E.optional E.guid   "by"     error.By
      |> E.optional E.guid   "target" error.Target
      |> E.optional E.string "on"     error.On
      |> E.optional E.string "why"    error.Reason
