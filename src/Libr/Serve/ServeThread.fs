namespace Forum

open Chessie.ErrorHandling

open Unif.Errors
open Unif.ForumModels
open Unif.MainModels

module ServeThread =

  open Utilities
  open DocReadWrite
  open ServeUser
  open ServePost

  let internal getThread threadId =
    Read.load<ForumThread> threadId
    |> failIfNone (NonExistentThread threadId)

  let internal toListedThread (thread : ForumThread) =
    trial {
      let! op       = getShownUser thread.Author
      let! lastUser =
        match thread.Replies.Length with
        | x when x <= 0 -> ShownUser.Empty |> ok
        | _ -> getPostsAuthor <| List.last thread.Replies
      return
        { Id         = thread.Id
          Title      = thread.Title
          OP         = op
          Replies    = thread.Replies.Length
          LastPoster = lastUser.Username
          LastActive = thread.LastActive }
    }

  let private toShownThread (thread : ForumThread) =
    trial {
      let  id       = thread.Id
      let  title    = thread.Title
      let! op       = getShownUser thread.Author
      let  status   = thread.Activity
      let  repcount = thread.Replies.Length
      let! reps     = List.map getShownPost thread.Replies |> collect
      return
        ShownThread.Create (id, title, op, status, repcount, reps)
    }

  let private chunkThread page perPage (thread : ForumThread) =
    chunkList page perPage thread.Id thread.Replies
    |> lift (flip setThreadReplies thread)

  let getListedThread threadId =
    getThread threadId
    >>= toListedThread

  let getShownThread threadId =
    getThread threadId
    >>= toShownThread

  let getApiShownThread threadId =
    getThread threadId
    |> lift ApiShownThread.CreateFromForumThread

  let getChunkedShownThread page perPage threadId =
    getThread threadId
    >>= chunkThread page perPage
    >>= toShownThread

  let getChunkedApiShownThread page perPage threadId =
    getThread threadId
    >>= chunkThread page perPage
    |>  lift ApiShownThread.CreateFromForumThread
