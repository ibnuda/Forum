module ManageTests

open System

open Xunit
open Chessie.ErrorHandling

open Unif.Errors
open Unif.ForumModels
open Unif.MainModels

open Forum.ForumModify
open Forum.BusinessDefaults
open Forum.DocReadWrite
open Forum.ManageUser
open Forum.ManagePost
open Forum.ManageThread


[<Fact>]
let CreateUserAndSaveTest () =
  ()
  let gidA = Guid.NewGuid ()
  let gidB = Guid.NewGuid ()
  let userA = ForumUser.Create (gidA, "MarcoPollo", "where is Marco?", normaluser)
  let shownUserA = ShownUser.CreateFromForumUser userA
  let createdUser = createUser gidA "MarcoPollo" "where is Marco?"
  let createdUserFail = createUser gidB "MarcoPollo" "something"

  let savedUser =
    Read.load<ForumUser> gidA
    |> failIfNone (NonExistentUser gidA)
    |> lift ShownUser.CreateFromForumUser

  Write.deleteEntity userA

  Assert.Equal (ok shownUserA, createdUser)
  Assert.Equal (ok shownUserA, savedUser)
  Assert.Equal (UsernameAlreadyExists "MarcoPollo" |> fail, createdUserFail)

[<Fact>]
let QualificationModificationTests () =
  let adm = ForumUser.Create (Guid.NewGuid (), "AdminUser", "Password", admin)
  let sen = ForumUser.Create (Guid.NewGuid (), "SeniorUser1", "Password", senmod)
  let anS = ForumUser.Create (Guid.NewGuid (), "SeniorUser2", "Password", senmod)
  let moD = ForumUser.Create (Guid.NewGuid (), "ModUser1", "Password", moderator)
  let anM = ForumUser.Create (Guid.NewGuid (), "ModUser2", "Password", moderator)
  let noA = ForumUser.Create (Guid.NewGuid (), "NormalUserA", "Pass", normaluser)
  let noB = ForumUser.Create (Guid.NewGuid (), "NormalUserB", "Pass", normaluser)
  let noC = ForumUser.Create (Guid.NewGuid (), "NormalUserC", "Pass", normaluser)
  let ban = ForumUser.Create (Guid.NewGuid (), "BannedUser", "Pass", banned)

  let users = Write.writeEntities [ adm; sen; anS; moD; anM; noA; noB; noC; ban ]

  let noCPlusSenMod           = setUserQualification senmod noC
  let grantSenModToNoC        = makeSenModerator adm.Id noC.Id
  let grantSenModToNoB        = makeSenModerator ban.Id noB.Id
  let noCPlusMod              = setUserQualification moderator noC
  let grantModToNoC           = makeModerator adm.Id noC.Id
  let grantModToBan           = makeModerator ban.Id ban.Id
  let normalPlusBan           = setUserQualification banned noB
  let banPlusRelease          = setUserQualification normaluser ban
  let releaseBanToBan         = makeUserFree moD.Id ban.Id
  let releaseBanToNoAByMod    = makeUserFree moD.Id noA.Id
  let releaseBanToNoAByBanned = makeUserFree ban.Id noA.Id

  Assert.Equal (ShownUser.CreateFromForumUser noCPlusSenMod |> ok, grantSenModToNoC)
  Assert.Equal (NotQualifiedToChangeQualification (noB.Username, senmod.Name) |> fail, grantSenModToNoB)
  Assert.Equal (ShownUser.CreateFromForumUser banPlusRelease |> ok, releaseBanToBan)
  Assert.Equal (CannotUnbanNonBannedUser noA.Id |> fail, releaseBanToNoAByMod)
  Assert.Equal (NotQualifiedToChangeQualification (noA.Username, normaluser.Name) |> fail, releaseBanToNoAByBanned)

  Write.deleteEntities [ adm; sen; anS; moD; anM; noA; noB; noCPlusSenMod; banPlusRelease ]

[<Fact>]
let CreateForumPostTest () =
  let user = ForumUser.Create (Guid.NewGuid(), "Username", "Password", normaluser)
  Write.writeEntity user |> ignore
  let expectedPost =
    ForumPost.Create (Guid.NewGuid (),
                      user.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 420.,
                      None,
                      "Blaze it!")
  let createdPost =
    createPost user.Id
               expectedPost.Id
               (DateTime.MinValue + TimeSpan.FromMinutes 420.)
               None
               "Blaze it!"
  Assert.Equal (ok expectedPost, createdPost)
  Write.deleteEntity user
  Write.deleteEntity expectedPost

[<Fact>]
let EditPostTest () =
  let user =
    ForumUser.Create (Guid.NewGuid(),
                      "Username",
                      "Password",
                      normaluser)
    |> Write.writeEntity
  let expectedPost =
    ForumPost.Create (Guid.NewGuid(),
                      user.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 420.,
                      None,
                      "Blaze it!")
  let ePost =
    expectedPost
    |> setPostTitle (Some "420")
    |> setPostContent "Smoke weed everyday"

  let createdPost =
    createPost user.Id
               expectedPost.Id
               (DateTime.MinValue + TimeSpan.FromMinutes 420.)
               None
               "Blaze it!"
  let editedPost =
    editPost expectedPost.Author
             expectedPost.Id
             (Some "420")
             "Smoke weed everyday"

  Write.deleteEntity user
  Write.deleteEntity ePost

  Assert.Equal (ok ePost, editedPost)

[<FactAttribute>]
let CanCreateThreadFromPost () =
  let user =
    ForumUser.Create (Guid.NewGuid (), "UserCreateThread", "Pass", normaluser)
    |> Write.writeEntity
  let anotherUser =
    ForumUser.Create (Guid.NewGuid (), "UserCreateThread", "Pass", normaluser)
    |> Write.writeEntity
  let userBanned =
    ForumUser.Create (Guid.NewGuid (), "UserCreateThread", "Pass", banned)
    |> Write.writeEntity
  let post : ForumPost =
    ForumPost.Create (Guid.NewGuid (),
                      user.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 420.,
                      Some "420 BLAZE IT!!!",
                      "Smoke weed everyday!")
    |> Write.writeEntity
  let cate =
    ForumCategory.Create (Guid.NewGuid (), "Cat", false, "Desc", [], 1)
    |> Write.writeEntity
  let anotherCate =
    ForumCategory.Create (Guid.NewGuid (), "Cat", false, "Desc", [], 2)
    |> Write.writeEntity
  let thread =
    ForumThread.Create (Guid.NewGuid (),
                        cate.Id, "420 BLAZE IT!!!",
                        user.Id,
                        Activity.Create (1,
                                         false,
                                         DateTime.MinValue +  TimeSpan.FromMinutes 420.,
                                         user.Id),
                        post.Created,
                        post.Author,
                        [ post.Id ])
    |> Write.writeEntity

  let okCreated =
    initThread user.Id
               thread.Id
               cate.Id
               (Some "420 BLAZE IT!!!")
               post.Id
  let failCreatedNotAuthor =
    initThread anotherUser.Id
               thread.Id
               cate.Id
               (Some "420 BLAZE IT!!!")
               post.Id
  let failCreatedNotQualif =
    initThread user.Id
               thread.Id
               anotherCate.Id
               (Some "420 BLAZE IT!!!")
               post.Id
  let failCreatedBanned =
    initThread userBanned.Id
               thread.Id
               anotherCate.Id
               (Some "420 BLAZE IT!!!")
               post.Id

  Assert.Equal (thread |> ok, okCreated)
  Assert.Equal (NotTheAuthor (anotherUser.Id, post.Id) |> fail, failCreatedNotAuthor)
  Assert.Equal (NotQualifiedToCreateThreadInCategory (user.Username, anotherCate.Name) |> fail,
                failCreatedNotQualif)
  Assert.Equal (YouAreBanned |> fail, failCreatedBanned)

  Write.deleteEntities [ user; anotherUser; userBanned ]
  Write.deleteEntities [ cate; anotherCate ]
  Write.deleteEntity post
  Write.deleteEntity thread

[<FactAttribute>]
let CanReplyThread () =
  let adm =
    ForumUser.Create (Guid.NewGuid (), "UserCreateThread", "Pass", admin)
    |> Write.writeEntity
  let user =
    ForumUser.Create (Guid.NewGuid (), "UserCreateThread", "Pass", normaluser)
    |> Write.writeEntity
  let post =
    ForumPost.Create (Guid.NewGuid (),
                      user.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 420.,
                      Some "420 BLAZE IT!!!",
                      "Smoke weed everyday!")
    |> Write.writeEntity
  let postByAdm =
    ForumPost.Create (Guid.NewGuid (),
                      adm.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 420.,
                      Some "Admin 420 BLAZE IT!!!",
                      "Smoke weed everyday!")
    |> Write.writeEntity
  let cate =
    ForumCategory.Create (Guid.NewGuid (), "Cat", false, "Desc", [], 1)
    |> Write.writeEntity
  let anotherCate =
    ForumCategory.Create (Guid.NewGuid (), "Mod Category", false, "Desc", [], 2)
    |> Write.writeEntity
  let thread =
    ForumThread.Create (Guid.NewGuid (),
                        cate.Id,
                        "420 BLAZE IT!!!",
                        user.Id,
                        Activity.Create (1,
                                         false,
                                         DateTime.MinValue + TimeSpan.FromSeconds 420.,
                                         user.Id),
                        post.Created,
                        post.Author,
                        [ post.Id ])
    |> Write.writeEntity
  let threadByAdmInModCat =
    ForumThread.Create (Guid.NewGuid (),
                        anotherCate.Id, "Admin 420 BLAZE IT!!!",
                        adm.Id,
                        Activity.Create (1,
                                         false,
                                         DateTime.MinValue + TimeSpan.FromMinutes 420.,
                                         adm.Id),
                        post.Created,
                        post.Author,
                        [ postByAdm.Id ])
    |> Write.writeEntity
  let postReply =
    ForumPost.Create (Guid.NewGuid (),
                      user.Id,
                      DateTime.MinValue + TimeSpan.FromMinutes 421.,
                      Some "420 BLAZE IT!!!",
                      "Smoke weed everyday!")
    |> Write.writeEntity
  let expReplied =
    Forum.ForumModify.replyThread postReply thread

  let okReplied = replyThread user.Id postReply.Id thread.Id
  let failRepliedNotQualified = replyThread user.Id postReply.Id threadByAdmInModCat.Id

  Assert.Equal (expReplied |> ok, okReplied)
  Assert.Equal (NotQualifiedToPostInThisForumThread (anotherCate.Id, user.Qualification.Name) |> fail,
                failRepliedNotQualified)

  Write.deleteEntity adm
  Write.deleteEntity user
  Write.deleteEntity post
  Write.deleteEntity postByAdm
  Write.deleteEntity postReply
  Write.deleteEntity cate
  Write.deleteEntity anotherCate
  Write.deleteEntity expReplied
  Write.deleteEntity threadByAdmInModCat
