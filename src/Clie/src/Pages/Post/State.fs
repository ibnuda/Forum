module Clie.Pages.Post.State

open System

open Fable.Core
open Fable.Core.JsInterop
open Fable.Import

open Fable.PowerPack
open Fable.PowerPack.Fetch.Fetch_types

open Elmish

open Clie.Utility.Utility
open Clie.Messages

open Types

let getP pid =
  let getpost = getItem "posts"
  Cmd.ofPromise getpost pid PostMessages.PostGet PostMessages.NotFound

let init pid : PostDataModel * Cmd<PostMessages> =
  PostDataModel.Empty,
  getP pid

let update (message : PostMessages) (model : PostDataModel) =
  match message with
  | PostMessages.PostGet postjson ->
    let post = ofJson<Post> postjson
    setpos post model,
    [ ]
  | PostMessages.NotFound ex ->
    seterr ex.Message model,
    [ ]
