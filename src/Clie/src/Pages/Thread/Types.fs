module Clie.Pages.Thread.Types

open System

open Clie.Global

open Clie.Pages.Post.Types

type Thread =
  { id      : string
    title   : string
    status  : Activity
    replies : string list }
  static member Empty =
    { id      = ""
      title   = "empty"
      status  = Activity.Empty
      replies = [ ] }

type ThreadModel =
  { ErrMessage : string
    ThreadData : Thread
    PostsData  : Post list}
  static member Empty =
    { ErrMessage = ""
      ThreadData = Thread.Empty
      PostsData  = [ ] }

let deleteThrReply pid model =
  { model with
      replies = List.filter (fun x -> x <> pid) model.replies }

let deletereply pid model =
  { model with
      ThreadData = deleteThrReply pid model.ThreadData }

let seterror error model =
  { model with ErrMessage = error }

let setthread thread model =
  { model with ThreadData = thread }

let setpostappend post model =
  let posts = List.append model.PostsData [ post ]
  { model with PostsData = posts }

let schwampthing post pid model =
  { model with
      ThreadData = deleteThrReply pid model.ThreadData
      PostsData  = List.append model.PostsData [ post ] }
