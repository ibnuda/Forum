module Clie.Types

open System

open Global
open Messages

open Clie.Pages

type Submodel =
  | Nothing
  | SMLogin    of Pages.Login.Types.LoginModel
  | SMCategory of Pages.Category.Types.CategoryModel
  | SMShownCat of Pages.ShownCat.Types.ShownCatModel
  | SMThread   of Pages.Thread.Types.ThreadModel
  | SMPost     of Pages.Post.Types.PostDataModel

type AppModel =
  { Page     : Page
    Menu     : Menu.Types.MenuModel
    Submodel : Submodel }

let setpage page model =
  { model with Page = page }

let setsubm subm model =
  { model with Submodel = subm }

let setmenu menu model =
  { model with Menu = menu }
