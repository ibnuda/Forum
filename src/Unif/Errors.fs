namespace Unif

open System

module Errors =

  /// Failure type.
  /// Only use ZZZ for debugging.
  /// Otherwise, use TFIsGoingOn
  type Failures =
    | AlreadyAModInCategory                of Guid   * string
    | AlreadyBanned                        of Guid
    | AlreadyParentlessCategory            of Guid
    | Argument
    | ArgumentNull                         of ArgumentNullException
    | CannotCreateThreadInLockedCategory   of string
    | CannotLockLockedThread               of Guid
    | CannotReplyLockedThread              of Guid
    | CannotSelfBan
    | CannotUnbanNonBannedUser             of Guid
    | CannotUnlockNotLockedThread          of Guid
    | CategoryAlreadyLocked                of Guid
    | CategoryAlreadyUnlocked              of Guid
    | DirectoryNotFound
    | EmptyUpload
    | EmptyQuery
    | Encryption
    | FailureOfDeserialization             of string
    | InputOutput
    | Integrity
    | InvalidAlgorithm
    | InvalidCommand                       of string
    | InvalidId                            of string
    | InvalidPage                          of string
    | Lancang
    | NonExistentCategory                  of Guid
    | NonExistentPost                      of Guid
    | NonExistentUsername                  of string
    | NonExistentThread                    of Guid
    | NonExistentUser                      of Guid
    | NotAModInCategory                    of Guid   * string
    | NotAUser                             of Guid
    | NotAValidPayload                     of string
    | NotNeeded
    | NotQualifiedToAddMod                 of Guid
    | NotQualifiedToChangePassword         of Guid
    | NotQualifiedToChangeQualification    of string * string
    | NotQualifiedToCreateCategory         of Guid   * string
    | NotQualifiedToCreateThreadInCategory of string * string
    | NotQualifiedToEditPost               of Guid   * Guid
    | NotQualifiedToLockThread             of Guid   * string
    | NotQualifiedToLockCategory           of Guid   * string
    | NotQualifiedToUnlockCategory         of Guid   * string
    | NotQualifiedToMoveCategory           of Guid   * Guid * Guid
    | NotQualifiedToFreeCategory           of Guid
    | NotQualifiedToPostInThisForumThread  of Guid   * string
    | NotQualifiedToUnlockThread           of Guid   * string
    | NotSupported
    | NotTheAuthor                         of Guid   * Guid
    | NullReference
    | ObjectDisposed
    | OutOfMemory
    | PathTooLong
    | PostAuthorAndPosterNotTheSame        of Guid   * Guid
    | SpecifiedPageNotFound                of Guid   * int
    | TFIsGoingOn
    | TFAreYou
    | TokenExpired
    | TokenNotKosher
    | TokenNotValid
    | Unauthorized
    | UsernameAlreadyExists                of string
    | UsernameOrPasswordIsNotCorrect
    | YouAreBanned
    /// Only use ZZZ for debugging.
    | ZZZ of Exception
