module ForumModify.Tests

open System
open Xunit

open Chessie.ErrorHandling

open Unif.ForumModels
open Forum.ForumModify

open Data

[<Fact>]
let CanLockForumThread () =
  let lockedThread = setThreadActivity (standardLocked (DateTime.FromBinary(11L)) anAdministrator.Id) aRepliedThread
  Assert.Equal (aLockedThread, lockedThread)

[<Fact>]
let CanCreateForumThreadFromAForumPost () =
  let createdThread = createThreadFromPost guidD Guid.Empty None posts.Head
  Assert.Equal (anOpenThreadX, createdThread)


