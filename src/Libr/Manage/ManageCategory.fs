namespace Forum

open Chessie.ErrorHandling
open Marten

open Unif.Errors
open Unif.ForumModels

module ManageCategory =

  open BusinessRules
  open DocHelpers
  open DocReadWrite

  open ManageThread

  let initCategory creatorId catId name desc mods minReq =
    match Read.load<ForumUser> creatorId with
    | None      -> NonExistentUser creatorId |> fail
    | Some user ->
      createCategory user catId name desc mods minReq
      |>  lift Write.writeEntity

  let addModAndSave actorId candidateId categoryId =
    match Read.load<ForumUser> actorId,
          Read.load<ForumUser> candidateId,
          Read.load<ForumCategory> categoryId with
    | None, _, _                                -> NonExistentUser actorId |> fail
    | Some _, None, _                           -> NonExistentUser candidateId |> fail
    | Some _, Some _, None                      -> NonExistentCategory categoryId |> fail
    | Some actor, Some candidate, Some category ->
      addModeratorInCategory actor candidate category
      |> lift Write.writeEntity

  let removeModAndSave actorId candidateId categoryId =
    match Read.load<ForumUser> actorId,
          Read.load<ForumUser> candidateId,
          Read.load<ForumCategory> categoryId with
    | None, _, _                                -> NonExistentUser actorId |> fail
    | Some _, None, _                           -> NonExistentUser candidateId |> fail
    | Some _, Some _, None                      -> NonExistentCategory categoryId |> fail
    | Some actor, Some candidate, Some category ->
      removeModeratorInCategory actor candidate category
      |> lift Write.writeEntity

  let depriveModAndRemoveFromCat actorId modId =
    match Read.load<ForumUser> actorId,
          Read.load<ForumUser> modId with
    | None, _                     -> (NotNeeded |> fail), (NonExistentUser actorId |> fail)
    | Some _, None                -> (NotNeeded |> fail), (NonExistentUser modId |> fail)
    | Some actor, Some moderator  ->
      let catWhichModdedByModerator =
        use session = store.QuerySession ()
        session
        |> Session.query<ForumCategory>
        |> Queryable.orderBy <@ fun category -> category.Name @>
        |> Seq.toList
      let (resultCats, removedMod) =
        depriveModerationAndRemoveFromCategories actor moderator catWhichModdedByModerator
      let resC = resultCats |> lift Write.writeEntities
      let resM = removedMod |> lift Write.writeEntity
      resC, resM

  let lockCategory actorId categoryId =
    match Read.load<ForumUser> actorId,
          Read.load<ForumCategory> categoryId with
    | None, _                  -> NonExistentUser actorId |> fail
    | Some _, None             -> NonExistentCategory categoryId |> fail
    | Some user, Some category ->
      lockCategory user category
      |> lift Write.writeEntity

  let lockCategoryBrutally actorId categoryId =
    match Read.load<ForumUser> actorId,
          Read.load<ForumCategory> categoryId with
    | None, _                  -> NonExistentUser actorId |> fail,
                                  NotNeeded |> fail
    | Some _, None             -> NonExistentCategory categoryId |> fail,
                                  NotNeeded |> fail
    | Some user, Some category ->
      match BusinessRules.lockCategory user category with
      | Ok (lockedCat, _) ->
        let locked =
          use session = store.LightweightSession ()
          session
          |> Session.query<ForumThread>
          |> Queryable.filter <@ fun t -> t.CategoryId = category.Id @>
          |> Seq.map (fun t -> lockThread user.Id t.Id)
          |> collect
          |> lift Write.writeEntities
        Write.writeEntity lockedCat |> ok,
        locked
      | Bad err -> err.Head |> fail,
                   NotNeeded |> fail

  let unlockCategory actorId categoryId =
    match Read.load<ForumUser> actorId,
          Read.load<ForumCategory> categoryId with
    | None, _                  -> NonExistentUser actorId |> fail
    | Some _, None             -> NonExistentCategory categoryId |> fail
    | Some user, Some category ->
      unlockCategory user category
      |> lift Write.writeEntity

  let unlockCategoryBrutally actorId categoryId =
    match Read.load<ForumUser> actorId,
          Read.load<ForumCategory> categoryId with
    | None, _                  -> NonExistentUser actorId |> fail,
                                  NotNeeded |> fail
    | Some _, None             -> NonExistentCategory categoryId |> fail,
                                  NotNeeded |> fail
    | Some user, Some category ->
      match BusinessRules.unlockCategory user category with
      | Ok (unlockedCat, _) ->
        let unlocked =
          use session = store.LightweightSession ()
          session
          |> Session.query<ForumThread>
          |> Queryable.filter <@ fun t -> t.CategoryId = category.Id @>
          |> Seq.map (fun thread -> unlockThread user.Id thread.Id)
          |> collect
          |> lift Write.writeEntities
        Write.writeEntity unlockedCat |> ok,
        unlocked
      | Bad err -> err.Head |> fail,
                   NotNeeded |> fail
