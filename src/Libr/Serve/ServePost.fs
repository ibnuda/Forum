namespace Forum

open Chessie.ErrorHandling

open Unif.Errors
open Unif.ForumModels
open Unif.MainModels

module ServePost =

  open DocReadWrite
  open ServeUser

  let toShownPost (post : ForumPost) =
    trial {
      let id      = post.Id
      let! op     = getShownUser post.Author
      let created = post.Created
      let title   = post.Title
      let content = post.Content
      return
        ShownPost.Create (id, op, created, title, content)
    }

  let getShownPost postId =
    Read.load<ForumPost> postId
    |>  failIfNone (NonExistentPost postId)
    >>= toShownPost

  let getPostsAuthor postId =
    Read.load<ForumPost> postId
    |>  failIfNone (NonExistentPost postId)
    >>= fun x -> getShownUser x.Author
