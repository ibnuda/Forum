namespace Forum

open System

open Chessie.ErrorHandling


module ImagesModels =

  open ConfigRead

  let internal config =
    match readConfig () with
    | Ok (r, _) -> r
    | Bad errs  -> failwithf "Config file is not valid: %A" errs.Head

  let defaultNotFound = config.DI.Empty

  type ImageInDb =
    { Id          : Guid
      Uploader    : Guid
      Name        : string
      ContentType : string
      Path        : string }
    static member Create (id, up, name, ctype, path) =
      { Id          = id
        Uploader    = up
        Name        = name
        ContentType = ctype
        Path        = path }
    static member Empty =
      ImageInDb.Create (Guid.Empty, Guid.Empty, "not found", "nothing", defaultNotFound)

  type ImageOutDb =
    { IdInDB : Guid
      Name : string
      FileItself : byte [] }
    static member Create (inDb, name, fileItself) =
      { IdInDB = inDb
        Name = name
        FileItself = fileItself }
