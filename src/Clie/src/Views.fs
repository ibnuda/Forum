module Clie.View

open Fable.Core
open Fable.Core.JsInterop

open Fable.Helpers.React
open Fable.Helpers.React.Props

open Elmish
open Elmish.React

open Global
open Messages
open Utility.Style
open Types
open State

importAll "../sass/main.sass"

let menuItem label page currentPage =
    li [ ] [
      a [ classList [ "is-active", page = currentPage ]
          Href (toHash page) ] [
        str label
      ]
    ]

let menu current =
  aside [ ClassName "menu" ] [
    p [ ClassName "menu-label" ] [
      str "General"
    ]
    ul [ ClassName "menu-list" ] [
      menuItem "Main" Main current
      menuItem "Sign Up" SignUp current
      menuItem "Login" Login current
    ]
  ]

let view model dispatch =
  let pageview model dispatch =
    match model.Page with
    | Page.Main ->
      div [ ClassName "has-text-centered"] [
        words 60 "Uwee!"
        hr [ ]
        a [ Href "https://ibnuda.gitlab.io" ] [
          words 20 "Heheh."
        ]
      ]
    | Page.Login ->
      match model.Submodel with
      | SMLogin loginmodel ->
        Pages.Login.Views.view loginmodel dispatch
      | _ -> div [ ] [ ]
    | Page.Category ->
      match model.Submodel with
      | SMCategory catmod ->
        Pages.Category.Views.view catmod dispatch
      | _ -> div [ ] [ ]
    | Page.CategoryId pid ->
      match model.Submodel with
      | SMShownCat catmod ->
        Pages.ShownCat.Views.view catmod dispatch
      | _ -> div [ ] [ ]
    | Page.Thread tid ->
      match model.Submodel with
      | SMThread thrmod ->
        Pages.Thread.Views.view thrmod dispatch
      | _ -> div [ ] [ ]
    | Page.SinglePost pid ->
      match model.Submodel with
      | SMPost post ->
        Pages.Post.Views.view post dispatch
      | _ -> div [ ] [ ]
    | Page.SignUp -> failwith "Not Implemented"
    | Page.UserInfo(_) -> failwith "Not Implemented"
  div [ ] [
    lazyView2 Pages.Menu.Views.view model.Menu dispatch
    div [ ClassName "section" ] [
      div [ ClassName "container" ] [
        div [ ClassName "notification is-centered" ] [
          pageview model dispatch
        ]
      ]
    ]
  ]
