module Clie.Pages.ShownCat.Views

open System

open Elmish

open Fable.Helpers.React
open Fable.Helpers.React.Props

open Clie.Global
open Clie.Messages

open Types

let view (model : ShownCatModel) dispatch =
  match String.IsNullOrEmpty model.ErrM with
  | false ->
    div [ ] [
      h3 [ ] [
        str model.ErrM
      ]
    ]
  | true ->
    div [ ClassName "container" ] [
      nav [ ClassName "breadcrumb" ] [
        li [ ] [
          a [ Href (toHash Page.Main) ] [ str "Home" ]
        ]
        li [ ] [
          a [ Href (toHash Page.Category) ] [ str "Categories" ]
        ]
        li [ ] [
          a [ Href (toHash <| Page.CategoryId model.Cats.id) ] [
            str model.Cats.name
          ]
        ]
      ]
      h4 [ ClassName "subtitle is-4 is-centered" ] [
        str model.Cats.desc
      ]
      table [ ClassName "table" ] [
        thead [ ] [
          tr [ ] [
            th [ ] [ text [ ] [ str "Title" ] ]
            th [ ] [ text [ ] [ str "OP" ] ]
            th [ ] [ text [ ] [ str "Qualif" ] ]
            th [ ] [ text [ ] [ str "Last Poster" ] ]
            th [ ] [ text [ ] [ str "Replies Count" ] ]
            th [ ] [ text [ ] [ str "Last Reply" ] ]
          ]
        ]
        tbody [ ] [
          if List.isEmpty model.Cats.threads then
            yield tr [ ] [ td [ ] [ text [ ] [ str "Empty" ] ] ]
          else
            for cat in model.Cats.threads do
              yield
                tr [ ] [
                  td [ ] [
                    a [ Href (toHash <| Page.Thread cat.id) ] [
                      str <| cat.title
                    ]
                  ]
                  td [ ] [ str cat.op.username ]
                  td [ ] [
                    a [ Href (toHash <| Page.UserInfo cat.op.id) ] [
                      str cat.op.username
                    ]
                  ]
                  td [ ] [ str <| cat.lastposter ]
                  td [ ] [ str <| cat.replies.ToString () ]
                  td [ ] [ str <| cat.lastact.ToString () ]
                ]
        ]
      ]
    ]
