# TODO

# Common
- Pinned threads, I guess.

## `Auth` module.

## `Busi` module.

## `Conf` module.

## `Fore` module.

## `Imag` module.

## `Lock` module.
- Hash password my dude. 
## `Real` module.

## `Save` module.
 - Fix this:
```
  let a = getCategories <@ fun category -> List.contains moderator.Id category.Moderatrolls @> 
```
- Which results:
```
 Unable to cast object of type 'System.Linq.Expressions.PropertyExpression' to type 'System.Linq.Expressions.LambdaExpression'.
Stack Trace:
   at Forum.Save.Lambda.translateExpr(Expression linq) in Forum\src\Save\Lib.fs:line 43
   at Forum.Save.Lambda.translateExpr(Expression linq) in Forum\src\Save\Lib.fs:line 44
   at Forum.Save.Doc.filter[a](FSharpExpr`1 f, IQueryable`1 q) in Forum\src\Save\Lib.fs:line 110
   at Forum.Fore.CategoryManagement.depriveModAndRemoveFromCat(Guid actorId, Guid modId) in Forum\src\Fore\CategoryManagement.fs:line 60
   at CategoryManagementTests.CanDepriveModFromCat() in Forum\tests\Fore.Tests\CategoryManagementTests.fs:line 66
``` 
- Well, just a workaround by skipping loading all categories and then filter the results.

## `Serv` module.
[] What should the server do when accept POST and cool?

## `Util` module.

## `tests` modules.
