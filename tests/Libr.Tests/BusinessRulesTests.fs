module BusinessRulesTests

open System

open Xunit
open Chessie.ErrorHandling

open Unif.Errors
open Unif.ForumModels

open Forum.Utilities
open Forum.ForumModify

open Forum.BusinessRules

open Data

[<Fact>]
let AdminCanBanAnyone () =
  let bannedNormalUser = banUser anAdministrator aNormalUser
  let bannedModeratroll = banUser anAdministrator aModerator
  Assert.Equal (ok { aNormalUser with Qualification = standardBanned }, bannedNormalUser)
  Assert.Equal (ok { aModerator with Qualification = standardBanned }, bannedModeratroll)

[<Fact>]
let ModeratrollCanBanOnlyNormalUser () =
  let bannedAdministrator = banUser aModerator anAdministrator
  let bannedModerator = banUser aModerator anotherModerator
  let bannedNormalUser = banUser aModerator aNormalUser
  Assert.Equal (NotQualifiedToChangeQualification (anAdministrator.Username, standardBanned.Name) |> fail, bannedAdministrator)
  Assert.Equal (NotQualifiedToChangeQualification (anotherModerator.Username, standardBanned.Name) |> fail, bannedModerator)
  Assert.Equal (ok { aNormalUser with Qualification = standardBanned }, bannedNormalUser)

[<Fact>]
let CannotBanSelf () =
  let bannedAdministrator = banUser anAdministrator anAdministrator
  Assert.Equal (fail CannotSelfBan, bannedAdministrator)

[<Fact>]
let BannedAndJailedUserCannotCreateAPost () =
  let postByBanned = createForumPost aBannedUser guidB (DateTime.FromBinary(10L)) None "This is a thread's title."
  let postByJailed = createForumPost aJailedUser guidB (DateTime.FromBinary(10L)) None "This is a thread's title."
  Assert.Equal (fail YouAreBanned, postByBanned)
  Assert.Equal (fail YouAreBanned, postByJailed)

[<Fact>]
let OnlyRightfulUserCanEditTheirPost () =
  let testPost =
    ForumPost.Create (guidA, aNormalUser.Id, (DateTime.FromBinary(10L)), Some "title", "Content.")
  let expGoodRes =
    ForumPost.Create (guidA, aNormalUser.Id, (DateTime.FromBinary(10L)), None, "")
  let expBadRes =
    NotQualifiedToEditPost (guidA, anotherNormalUser.Id)
  let anEditedPostByANormalUser = editForumPost aNormalUser testPost None ""
  let anEditedPostByAnotherNormalUser = editForumPost anotherNormalUser testPost None ""
  Assert.Equal (ok expGoodRes, anEditedPostByANormalUser)
  Assert.Equal (fail expBadRes, anEditedPostByAnotherNormalUser)

[<Fact>]
let CanReplyOpenedForumThread () =
  let repliedThread =
    posts.Tail
    |> List.fold (fun thread post ->
                  match thread with
                  | Ok (thread, _) ->
                    replyForumThread aNormalUser post thread
                  | Bad x -> x.Head |> fail) (ok anOpenThread)
  Assert.Equal (ok aRepliedThread, repliedThread)

[<Fact>]
let CannotReplyOpenedForumThreadWithSomebodyElsesPost () =
  let repliedThread =
    posts.Tail
    |> List.fold (fun thread post ->
                  match thread with
                  | Ok (thread, _) ->
                    replyForumThread anAdministrator post thread
                  | Bad x -> x.Head |> fail) (ok anOpenThread)
  Assert.Equal (PostAuthorAndPosterNotTheSame (aNormalUser.Id, anAdministrator.Id) |> fail, repliedThread)

[<Fact>]
let CannotReplyLockedForumThread () =
  let repliedThread =
    posts.Tail
    |> List.fold (fun thread post ->
      match thread with
      | Ok (thread, _) ->
        replyForumThread aNormalUser post thread
      | Bad _ -> [ CannotReplyLockedThread (aLockedThread.Id) ] |> Bad ) (ok aLockedThread)
  Assert.Equal (CannotReplyLockedThread (aLockedThread.Id) |> fail, repliedThread)

[<Fact>]
let OnlyAdminAndModeratrollCanLockAThread () =
  let lockedThreadByAdministrator = lockForumThread (DateTime.FromBinary(11L)) anAdministrator anOpenThread
  Assert.Equal (ok { anOpenThread with Activity = standardLocked (DateTime.FromBinary(11L)) anAdministrator.Id },
                lockedThreadByAdministrator)
  let lockedThreadByModeratroll = lockForumThread (DateTime.FromBinary(11L)) aModerator anOpenThread
  Assert.Equal (ok { anOpenThread with Activity = standardLocked (DateTime.FromBinary(11L)) aModerator.Id },
                lockedThreadByModeratroll)
  let lockedThreadByNormalUser = lockForumThread (DateTime.FromBinary(11L)) aNormalUser anOpenThread
  Assert.Equal (NotQualifiedToLockThread (anOpenThread.Id, aNormalUser.Qualification.Name) |> fail, lockedThreadByNormalUser)

[<Fact>]
let CannotLockAlreadyLockedThread () =
  let lockedThreadByAdministrator = lockForumThread (DateTime.FromBinary(11L)) anAdministrator aLockedThread
  Assert.Equal (CannotLockLockedThread aLockedThread.Id |> fail, lockedThreadByAdministrator)
  let lockedThreadByModeratroll = lockForumThread (DateTime.FromBinary(11L)) aModerator aLockedThread
  Assert.Equal (CannotLockLockedThread aLockedThread.Id |> fail, lockedThreadByModeratroll)

[<Fact>]
let OnlyAdminCanCreatedThread () =
  let catByAdmin = createCategory anAdministrator Guid.Empty "CatName" "CatDesc" [] 1
  let catByMod = createCategory aModerator Guid.Empty "CatName" "CatDesc" [] 1
  let catByUser = createCategory aNormalUser Guid.Empty "Catname" "CatDesc" [] 1
  Assert.Equal (ok aCat, catByAdmin)
  Assert.Equal (NotQualifiedToCreateCategory (aModerator.Id, aModerator.Qualification.Name) |> fail, catByMod)
  Assert.Equal (NotQualifiedToCreateCategory (aNormalUser.Id, aNormalUser.Qualification.Name) |> fail, catByUser)

[<Fact>]
let ModCanInCategoryByRighteousUser () =
  let addedModInCatByAdmin = addModeratorInCategory anAdministrator aModerator aCat
  let addedModInCatBySenMod = addModeratorInCategory aSeniorMod aModerator aCat
  let addedModInCatByMod = addModeratorInCategory anotherModerator aModerator aCat
  let addedModInCatByUser = addModeratorInCategory aNormalUser aModerator aCat
  Assert.Equal (ok aCatPlusMod, addedModInCatByAdmin)
  Assert.Equal (ok aCatPlusMod, addedModInCatBySenMod)
  Assert.Equal (NotQualifiedToAddMod anotherModerator.Id |> fail, addedModInCatByMod)
  Assert.Equal (NotQualifiedToAddMod aNormalUser.Id |> fail, addedModInCatByUser)

[<Fact>]
let CanDepriveMod () =
  let admin = ForumUser.Create (Guid.NewGuid (), "Adm", "Pas", standardAdministraTrollQualification)
  let nod = ForumUser.Create (Guid.NewGuid (), "Mod", "Pas", standardModeraTrollQualification)
  let catA = ForumCategory.Create (Guid.NewGuid(), "A", false, "DescrA", [ nod.Id ], 1)
  let catB = ForumCategory.Create (Guid.NewGuid(), "B", false, "DescrB", [ nod.Id ], 1)
  let catC = ForumCategory.Create (Guid.NewGuid(), "C", false, "DescrC", [ admin.Id ], 1)

  let newCatA = removeModInCategory nod catA
  let newCatB = removeModInCategory nod catB
  let notNod = { nod with Qualification = standardNormalUser }

  let (newCats, notMod) =
    depriveModerationAndRemoveFromCategories admin nod [ catA; catB ]

  Assert.Equal ( notNod |> ok, notMod)
  Assert.Equal ([ newCatA; newCatB ] |> ok, newCats)
