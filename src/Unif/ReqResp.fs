namespace Unif

open System

module ResponseModels =

  open Chessie.ErrorHandling

  open Errors
  open Utilities
  open MainModels

  type ForumResponse =
    | NormalString       of string
    | Empty              of string
    | DataUser           of ShownUser
    | DataPost           of ShownPost
    | DataPosts          of ListedPost list
    | DataListedThread   of ListedThread
    | DataListedThreads  of ListedThreadCompact list
    | DataStatusThread   of StatusThread
    | DataSingleThread   of ApiShownThread
    | DataMainCategories of ListedCategory list
    | DataListedCategory of ListedCategory
    | DataSingleCategory of ShownCategory
    | DataStatusCategory of StatusCategory
    | DataError          of ShownError list

  let resToJson (result : Result<ForumResponse, Failures>) =
    match result with
    | Ok (res, _) ->
      match res with
      | DataUser           su   -> modelToJson ShownUser.encode su
      | DataPost           dp   -> modelToJson ShownPost.encode dp
      | DataPosts          dps  -> modelToJson ListedPosts.encode dps
      | DataListedThread   dlt  -> modelToJson ListedThread.encode dlt
      | DataListedThreads  dlts -> modelToJson ListedThreadCompacts.encode dlts
      | DataSingleThread   dst  -> modelToJson ApiShownThread.encode dst
      | DataMainCategories dmc  -> modelToJson ListedCategories.encode dmc
      | DataListedCategory lc   -> modelToJson ListedCategory.encode lc
      | DataSingleCategory sc   -> modelToJson ShownCategory.encode sc
      | NormalString       s    -> s
      | DataStatusThread dst    -> modelToJson StatusThread.encode dst
      | DataStatusCategory dsc  -> modelToJson StatusCategory.encode dsc
      | Empty _                 -> ""
      | DataError _             -> "This should not be possible."
    | Bad errs    ->
      errs.Head
      |> ShownError.CreateFromFailure
      |> modelToJson ShownError.encode

module RequestModels =

  open Chiron
  open Operators

  module D = Json.Decode

  let dReqGuid = D.required D.guid
  let dOptGuid = D.optional D.guid
  let dOptStri = D.optional D.string
  let dReqStri = D.required D.string

  type UserCreate =
    { Username : string
      Password : string }
  module UserCreate =
    let private create u p = { Username = u; Password = p }
    let decode = create <!> dReqStri "username" <*> dReqStri "password"

  type ChangePass =
    { ExecId : Guid
      Target : Guid
      Passwo : string }
  module ChangePass =
    let private create e t p = { ExecId = e; Target = t; Passwo = p }
    let decode =
      create <!> dReqGuid "execid" <*> dReqGuid "target" <*> dReqStri "passwo"

  type Command =
    | GrantAdm
    | GrantSenMod
    | DepriveSenMod
    | GrantMod
    | DepriveMod
    | Ban
    | Release
    | LockThread
    | UnlockThread
    | Give4thJuly
    | ChangeParent
    | AddModCategory
    | RemModCategory
    | LockCategory
    | UnlockCategory
    | Fail
    static member FromString =
      function
      | x when x = "grantadm"       -> GrantAdm
      | x when x = "grantsen"       -> GrantSenMod
      | x when x = "deprisen"       -> DepriveSenMod
      | x when x = "grantmod"       -> GrantMod
      | x when x = "deprimod"       -> DepriveMod
      | x when x = "ban"            -> Ban
      | x when x = "release"        -> Release
      | x when x = "lockthread"     -> LockThread
      | x when x = "unlockthread"   -> UnlockThread
      | x when x = "give4thjuly"    -> Give4thJuly
      | x when x = "changeparent"   -> ChangeParent
      | x when x = "addmod"         -> AddModCategory
      | x when x = "remmod"         -> RemModCategory
      | x when x = "lockcategory"   -> LockCategory
      | x when x = "unlockcategory" -> UnlockCategory
      | _                           -> Fail

  type Manage =
    { Command : Command }
    static member CreateFromRequest c = { Command = Command.FromString c }
  module Manage =
    let decode =
      Manage.CreateFromRequest <!> dReqStri "command"

  type PostObj =
    { Title   : string option
      Content : string }
  module PostObj =
    let private create t c = { Title = t; Content = c }
    let decode =
      create <!> dOptStri "title" <*> dReqStri "content"

  type CategoryCreate =
    { Name   : string
      Desc   : string
      Mods   : Guid list
      MinReq : int }
  module CategoryCreate =
    let private create n d m r = { Name = n; Desc = d; Mods = m; MinReq = r }
    let decode =
      create
      <!> dReqStri   "name"
      <*> dReqStri   "desc"
      <*> D.required (D.listWith D.guid) "mods"
      <*> D.required D.int "minreq"

