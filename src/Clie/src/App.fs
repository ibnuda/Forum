module App

open Fable.Core
open Fable.Core.JsInterop

open Elmish
open Elmish.Debug
open Elmish.React
open Elmish.Browser.Navigation
open Elmish.Browser.UrlParser

open Clie.Global
open Clie.Types
open Clie.State
open Clie.View

Program.mkProgram init update view
|> Program.toNavigable (parseHash pageParser) urlUpdate
|> Program.withConsoleTrace
|> Program.withReact "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run
