module Clie.Pages.Menu.State

open Elmish

open Clie.Utility.Utility
open Clie.Messages

open Types

let init () = { User = load "user" }, Cmd.none

let update (mess : MenuMessages) (model : MenuModel) =
  match mess with
  | MenuLogin ->
    { model with User = load "user"},
    Cmd.none
  | MenuLogout ->
    { model with User = None },
    Cmd.none
