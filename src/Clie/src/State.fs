module Clie.State

open Fable.Core
open Fable.Core.JsInterop
open Fable.Import

open Elmish
open Elmish.Browser
open Elmish.Browser.Navigation
open Elmish.Browser.UrlParser

open Clie.Global
open Messages

open Clie.Pages

open Types

let pageParser : Parser<Page -> Page, Page> =
  oneOf [
    map Page.SignUp (s "signup")
    map Page.UserInfo (s "user" </> str)
    map Page.Main (s "main")
    map Page.Login (s "login")
    map Page.Category (s "category")
    map Page.CategoryId (s "category" </> str)
    map Page.Thread (s "thread" </> str)
    map Page.SinglePost (s "post" </> str)
  ]

let urlUpdate res (model : AppModel) =
  match res with
  | None ->
    model
    |> setpage Page.Main
    |> setsubm Nothing,
    Navigation.modifyUrl <| toHash Page.Main
  | Some Page.Login ->
    let loginmod, loginmsg = Login.State.init model.Menu.User
    model
    |> setpage Page.Login
    |> setsubm (SMLogin loginmod),
    Navigation.newUrl <| toHash Page.Login
  | Some Category ->
    let catmod, catcom = Category.State.init ()
    model
    |> setpage Page.Category
    |> setsubm (SMCategory catmod),
    Cmd.batch [
      Navigation.newUrl <| toHash Page.Category
      Cmd.map CategoryMsg catcom
    ]
  | Some (CategoryId cid as showncat ) ->
    let catmod, catcom = ShownCat.State.init <| Some cid
    model
    |> setpage showncat
    |> setsubm (SMShownCat catmod),
    Cmd.batch [
      Navigation.newUrl <| toHash showncat
      Cmd.map ShownCatMsg catcom
    ]
  | Some (Thread tid as thre) ->
    let thrmod, thrcom = Thread.State.init <| Some tid
    model
    |> setpage thre
    |> setsubm (SMThread thrmod),
    Cmd.batch [
      Navigation.newUrl <| toHash thre
      Cmd.map ThreadMsg thrcom
    ]
  | Some (SinglePost pid as post) ->
    let posmod, postcom = Post.State.init <| Some pid
    model
    |> setpage post
    |> setsubm (SMPost posmod),
    Cmd.batch [
      Navigation.newUrl <| toHash post
      Cmd.map PostMsg postcom
    ]
  | Some SignUp -> failwith "Not Implemented"
  | Some (UserInfo uid) -> failwith "Not"
  | Some Main -> failwith "Not"

let init res =
  let menu, menucom = Menu.State.init ()
  let m =
    { Page = Page.Main
      Menu = menu
      Submodel = Nothing }
  let m, com = urlUpdate res m
  m,
  Cmd.batch [
    com
    menucom
  ]

let update appmess appmod : AppModel * Cmd<AppMessage> =
  match appmess, appmod.Submodel with
  | Open, _ ->
    appmod
    |> setpage Page.Main
    |> setsubm Nothing,
    Navigation.modifyUrl <| toHash Page.Main
  | LoginMsg loginmes, SMLogin loginmod ->
    let loginmodel, logincommand = Login.State.update loginmes loginmod
    match loginmodel.State with
    | Login.Types.LoggedInState token ->
      let nuser = { Username = loginmodel.LoginReq.username; Token = token}
      let command =
        if appmod.Menu.User = Some nuser then
          Cmd.map LoginMsg logincommand
        else
          Cmd.batch [
            Cmd.map LoginMsg logincommand
            Cmd.ofFunc (Utility.Utility.save "tokn") token (fun _ -> LoggedIn) ShitHitsTheFan
            Cmd.ofFunc (Utility.Utility.save "user") nuser (fun _ -> LoggedIn) ShitHitsTheFan
            Navigation.newUrl <| toHash Page.Main
          ]
      appmod
      |> setsubm (SMLogin loginmodel)
      |> setmenu { appmod.Menu with User = Some nuser },
      command
    | Login.Types.LoggedOutState ->
      appmod
      |> setsubm (SMLogin loginmodel)
      |> setmenu { appmod.Menu with User = None },
      Cmd.map LoginMsg logincommand
  | Logout, _ ->
    match appmod.Menu.User with
    | Some thing ->
      let a, b = Menu.State.update MenuMessages.MenuLogout appmod.Menu
      appmod
      |> setmenu a
      |> setsubm Nothing,
      Cmd.batch [
        Navigation.newUrl <| toHash Page.Main
        b
        Cmd.ofFunc Utility.Utility.delete "tokn" (fun _ -> Logout) ShitHitsTheFan
        Cmd.ofFunc Utility.Utility.delete "user" (fun _ -> Logout) ShitHitsTheFan
      ]
    | None ->
      appmod, Cmd.none
  | CategoryMsg mes, SMCategory model ->
    let catmod, catcom = Category.State.update mes model
    appmod
    |> setpage Page.Category
    |> setsubm (SMCategory catmod),
    [ ]
  | ShownCatMsg mes, SMShownCat model ->
    let catmod, catcom = ShownCat.State.update mes model
    appmod
    |> setpage (Page.CategoryId catmod.Cats.id)
    |> setsubm (SMShownCat catmod),
    [ ]
  | ThreadMsg mes, SMThread model ->
    let thrmod, thrcom = Thread.State.update mes model
    appmod
    |> setpage (Page.Thread thrmod.ThreadData.id)
    |> setsubm (SMThread thrmod),
    Cmd.map ThreadMsg thrcom
  | PostMsg mes, SMPost model ->
    let posmod, poscom = Post.State.update mes model
    appmod
    |> setpage (Page.SinglePost posmod.PostData.id)
    |> setsubm (SMPost posmod),
    Cmd.map PostMsg poscom
  | _ ->
    appmod,
    Navigation.newUrl <| toHash Page.Main
